using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Planula.Models;
using Planula.Models.Config;
using Planula.Services.Config;
using Planula.Services.Crypto;
using Planula.Services.DB;
using Planula.Services.Miner;

namespace PlanulaTests.Services
{
    public class MinerServiceTests
    {
        #region Init

        public MinerServiceTests()
        {
            // TODO: ������ � IoC-���������
            _DBService = new DBService();
            var cryptoService = new CryptoService();
            var (priv, pub) = cryptoService.CreateKeys();
            var config = new AppConfig
            {
                Crypto = new CryptoConfig
                {
                    PrivateKey = priv,
                    PublicKey = pub,
                }
            };
            var cryptoConfigMock = new Mock<IConfigService>();
            cryptoConfigMock.Setup(_ => _.Config).Returns(config);

            _Miner = new MinerService(_DBService, cryptoService, cryptoConfigMock.Object);
        }

        [SetUp]
        public void Setup()
        {

        }

        #endregion Init

        #region Tests

        [Test]
        public void CreateNewBlock_Success()
        {
            var block = new ContentBlock
            {
                Content = "Some content",
                Created = new DateTime(TICKS),
            };
            var expected = new Block
            {
                Content = block,
                Author = "",
                Hash = "",
                Salt = "",
                Sign = "",
            };

            var result =  _Miner.CreateNewBlock(block);

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void Validate_Success()
        {
            var content = new ContentBlock
            {
                Content = "Some content",
                Created = new DateTime(TICKS),
            };
            var block = _Miner.CreateNewBlock(content);

            var result = _Miner.CheckBlock(block, out var errors);

            Assert.IsTrue(result);
        }

        #endregion Tests

        #region Consts

        private const long TICKS = 100000000;

        #endregion Consts

        #region Private fields

        private readonly IDBService _DBService;
        private readonly IMinerService _Miner;

        #endregion Private fields
    }
}