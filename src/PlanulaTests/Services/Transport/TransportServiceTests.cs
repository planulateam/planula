using NUnit.Framework;
using Planula.Models;
using Planula.Services.Config;
using Planula.Services.Crypto;
using Planula.Services.DB;
using Planula.Services.Manager;
using Planula.Services.Miner;
using Planula.Services.Transport;
using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using Planula.Services.Transport.Models.Messages;
using Planula.Services.Transport.Sender;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PlanulaTests.Services.Transport
{
    public class TransportServiceTests
    {
        private readonly IPAddress _ip = IPAddress.Parse("0.0.0.0");
        private readonly ushort _port = 1234;

        #region .Ctor

        public TransportServiceTests()
        {
            XPacketTypeManager.RegisterTypes();

            var config = new ConfigService();
            _Transport = new TransportService(config);
        }

        #endregion .Ctor

        [SetUp]
        public void Setup()
        {
            
            
        }

        [Test]
        public async Task SendBlock_InvalidBlock_BlockIsNotValidAsync()
        {
            var config = new ConfigService();
            var transport = new TransportService(config);
            var db = new DBService();
            var crypto = new CryptoService();
            var miner = new MinerService(db, crypto, config);
            var manager = new ManagerService(miner, crypto, transport, config, db);
            manager.Start();
            manager.CreateArticle();
            var block = new Block()
            {
                Author = "Author",
                Content = new ContentBlock()
                {
                    Content = "Content",
                    Created = DateTime.Now
                },
                Hash = "Hash",
                Salt = "Salt",
                Sign = "Sign"
            };
            var xBlock = new TPacketBlock
            {
                Block = JsonConvert.SerializeObject(block)
            };
            var xPacketOut = XPacketConverter.Serialize(XPacketType.NewBlock, xBlock);

            var packetIn = await new Sender(config).SendPacketAsync(_ip, _port, xPacketOut);

            var nodeResponse = XPacketConverter.Deserialize<TPacketResult>(packetIn);

            Assert.IsTrue(!nodeResponse.IsSuccess);
        }

        [Test]
        public void Listen_Default_Success()
        {
            var config = new ConfigService();
            var transport = new TransportService(config);

            Assert.IsTrue(transport.Listen(null, 1234));
        }

        [Test]
        public void Serialization_ValidPacket_Success()
        {
            var t = new TPacketTest
            {
                TestInt = 12345,
                TestDouble = 123.45D,
                TestBoolean = true,
                TestString = "teststr123"
            };

            var packet = XPacketConverter.Serialize(XPacketType.Handshake, t);
            var tDes = XPacketConverter.Deserialize<TPacketTest>(packet);

            Assert.IsTrue(t.TestBoolean == tDes.TestBoolean &&
                    t.TestDouble == tDes.TestDouble &&
                    t.TestInt == tDes.TestInt &&
                    t.TestString == tDes.TestString);
        }

        [Test]
        public void Serialization_TPacketBlock_Success()
        {
            var block = new Block()
            {
                Author = "Author",
                Content = new ContentBlock()
                {
                    Content = "Content",
                    Created = DateTime.Now
                },
                Hash = "Hash",
                Salt = "Salt",
                Sign = "Sign"
            };
            var blockStr = JsonConvert.SerializeObject(block);
            var xBlock = new TPacketBlock
            {
                Block = blockStr
            };

            var packet = XPacketConverter.Serialize(XPacketType.NewBlock, xBlock);
            var res = XPacketConverter.Deserialize<TPacketBlock>(packet);

            Assert.AreEqual(blockStr, res.Block);
        }

        [Test]
        public void Serialization_NotValidPacket_Error()
        {
            try
            {
                var t = new TPacketHandshake()
                {
                    MagicHandshakeNumber = 10
                };

                var packet = XPacketConverter.Serialize(XPacketType.NewBlock, t);
                var tDes = XPacketConverter.Deserialize<TPacketTest>(packet);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.IsTrue(false);
            }

        }


        [Test]
        public async Task Net_SendTestPacket_SuccessAsync()
        {
            var config = new ConfigService();
            var transport = new TransportService(config);
            var db = new DBService();
            var crypto = new CryptoService();
            var miner = new MinerService(db, crypto, config);
            var manager = new ManagerService(miner, crypto, transport, config, db);
            manager.Start();

            var ip = IPAddress.Parse("0.0.0.0");
            var port = 1234;

            var tPacketTest = new TPacketTest
            {
                TestInt = 12345,
                TestDouble = 123.45D,
                TestBoolean = true,
                TestString = "teststr123"
            };

            var xPacketOut = XPacketConverter.Serialize(XPacketType.Test, tPacketTest);

            var sender = new Sender(config);

            var packetIn = await sender.SendPacketAsync(ip, (ushort)port, xPacketOut);

            var testResponse = XPacketConverter.Deserialize<TPacketTest>(packetIn);



            Assert.IsTrue(tPacketTest.TestBoolean == testResponse.TestBoolean &&
                    tPacketTest.TestDouble == testResponse.TestDouble &&
                    tPacketTest.TestInt == testResponse.TestInt &&
                    tPacketTest.TestString == testResponse.TestString);
        }

        #region Private fields

        private readonly ITransportService _Transport;

        #endregion Private fields
    }
}