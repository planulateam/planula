using System;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Planula.Models;
using Planula.Services.Crypto;
using Planula.Services.DB;
using Planula.Services.Miner;

namespace PlanulaTests.Services
{
    public class CryptoServiceTests
    {
        #region Init

        public CryptoServiceTests()
        {
            _CryptoService = new CryptoService();
        }

        [SetUp]
        public void Setup()
        {

        }

        #endregion Init

        #region Tests

        [Test]
        public void CreateNewBlock_Success()
        {
            var str = "Hello world";
            var strData = Encoding.UTF8.GetBytes(str);

            var (privateKey, publicKey) = _CryptoService.CreateKeys();

            var sign = _CryptoService.ComputeSign(strData, privateKey);
            var result = _CryptoService.CheckSign(strData, sign, publicKey);

            Assert.IsTrue(result);
        }

        #endregion Tests

        #region Consts

        private const long TICKS = 100000000;

        #endregion Consts

        #region Private fields

        private readonly IDBService _DBService;
        private readonly ICryptoService _CryptoService;

        #endregion Private fields
    }
}