﻿using System;
using CLI.Helpers;
using Planula.Services.Config;
using Planula.Services.Crypto;
using Planula.Services.DB;
using Planula.Services.Log;
using Planula.Services.Manager;
using Planula.Services.Miner;
using Planula.Services.Transport;
using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Sender;

namespace Planula
{
    public class App
    {
        static void Main(string[] args)
        {

            XPacketTypeManager.RegisterTypes();

            var config = new ConfigService();
            var transport = new TransportService(config);
            var db = new DBService();
            var crypto = new CryptoService();
            var miner = new MinerService(db, crypto, config);
            var log = new LoggerService();
            var manager = new ManagerService(miner, crypto, transport, config, db, log);

            var builder = new CLIRunnerBuilder();
            var cli = builder
                .Init()
                .AddCommands(manager)
                .Build();

            cli.Run();
        }
    }
}