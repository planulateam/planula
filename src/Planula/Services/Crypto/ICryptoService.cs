﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;

namespace Planula.Services.Crypto
{
    /// <summary>
    ///     Сервис криптографии
    /// </summary>
    public interface ICryptoService
    {
        /// <summary>
        ///     Вычислить хеш
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <returns>Массив байт хеша</returns>
        byte[] ComputeHash(byte[] data);
        /// <summary>
        ///     Вычислить хеш
        /// </summary>
        /// <param name="stream">Поток байт исходных данных</param>
        /// <returns>Массив байт хеша</returns>
        byte[] ComputeHash(Stream stream);

        /// <summary>
        ///     Подписать данные
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <param name="privateKey">Ключ для подписи</param>
        /// <returns>Отсоединенная подпись</returns>
        byte[] ComputeSign(byte[] data, RSAParameters privateKey);
        /// <summary>
        ///     Проверить правильность подписи
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <param name="publicKey">Ключ для проверки подписи</param>
        /// <returns>Результат проверки подписи</returns>
        bool CheckSign(byte[] data, byte[] sign, RSAParameters publicKey);
        /// <summary>
        ///     Создать RSA-ключи (private / public)
        /// </summary>
        /// <returns>Пара ключей (private / public)</returns>
        (RSAParameters, RSAParameters) CreateKeys();


    }
}
