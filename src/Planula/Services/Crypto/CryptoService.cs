﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;

namespace Planula.Services.Crypto
{
    /// <summary>
    ///     Сервис криптографии
    /// </summary>
    public class CryptoService : ICryptoService
    {
        #region Public methods

        /// <summary>
        ///     Вычислить хеш
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <returns>Массив байт хеша</returns>
        public byte[] ComputeHash(byte[] data)
        {
            byte[] result = null;
            using (var sha256 = SHA256.Create())
            {
                result = sha256.ComputeHash(data, 0, data.Length);
            }
            return result;
        }
        /// <summary>
        ///     Вычислить хеш
        /// </summary>
        /// <param name="stream">Поток байт исходных данных</param>
        /// <returns>Массив байт хеша</returns>
        public byte[] ComputeHash(Stream stream)
        {
            byte[] result = null;
            using (var sha256 = SHA256.Create())
            {
                result = sha256.ComputeHash(stream);
            }
            return result;
        }

        /// <summary>
        ///     Подписать данные
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <param name="privateKey">Ключ для подписи</param>
        /// <returns>Отсоединенная подпись</returns>
        public byte[] ComputeSign(byte[] data, RSAParameters privateKey)
        {
            var hash = ComputeHash(data);
            byte[] sign = null;
            using (var csp = new RSACryptoServiceProvider())
            {
                csp.ImportParameters(privateKey);
                sign = csp.SignHash(hash, CryptoConfig.MapNameToOID(SHA_ALG));
            }
            return sign;
        }
        /// <summary>
        ///     Проверить правильность подписи
        /// </summary>
        /// <param name="data">Массив байт исходных данных</param>
        /// <param name="publicKey">Ключ для проверки подписи</param>
        /// <returns>Результат проверки подписи</returns>
        public bool CheckSign(byte[] data, byte[] sign, RSAParameters publicKey)
        {
            var hash = ComputeHash(data);
            bool result = false;

            using (var csp = new RSACryptoServiceProvider())
            {
                csp.ImportParameters(publicKey);
                result = csp.VerifyHash(hash, CryptoConfig.MapNameToOID(SHA_ALG), sign);
            }

            return result;
        }
        /// <summary>
        ///     Создать RSA-ключи (private / public)
        /// </summary>
        /// <returns>Пара ключей (private / public)</returns>
        public (RSAParameters, RSAParameters) CreateKeys()
        {
            RSAParameters privateKey;
            RSAParameters publicKey;
            using (var csp = new RSACryptoServiceProvider(KEY_SIZE))
            {
                privateKey = csp.ExportParameters(true);
                publicKey = csp.ExportParameters(false);
            }

            return (privateKey, publicKey);
        }

        #endregion Public methods

        #region Consts

        private const int KEY_SIZE = 2048;
        private const string SHA_ALG = "SHA256";

        #endregion Consts
    }
}