﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Planula.Attributes;
using Planula.Models;
using Planula.Models.Config;
using Planula.Providers.Mapping;
using Planula.Providers.Streams;

namespace Planula.Services.Config
{
    /// <summary>
    ///     Сервис конфигурации
    /// </summary>
    public class ConfigService : IConfigService
    {
        #region .Ctor

        public ConfigService()
        { }

        #endregion .Ctor

        #region Public properties

        /// <summary>
        ///     Конфигурация приложения
        /// </summary>
        public AppConfig Config { get; } = new AppConfig();

        #endregion Public properties

        #region Public methods

        /// <summary>
        ///     Загрузить все конфиги
        /// </summary>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Результат выполнения</returns>
        public bool Load(out List<Error> errors)
        {
            errors = null;
            var result = true;

            try
            {
                var configProps = typeof(AppConfig).GetProperties().Where(p => p.PropertyType.IsClass);
                foreach (var configProp in configProps)
                {
                    var type = configProp.PropertyType;
                    var name = type.GetCustomAttribute<ConfigDefaultAttribute>()?.Name;
                    if (name == null)
                    {
                        name = $"{type.Name}.json";
                    }
                    var config = Load(name, type, out var ConfigErrors);
                    if (config == null)
                    {
                        if (errors == null)
                        {
                            errors = new List<Error>();
                        }
                        errors.AddRange(ConfigErrors);
                        result = false;
                    }
                    configProp.SetValue(Config, config);
                }
            }
            catch (Exception e)
            {
                if (errors == null)
                {
                    errors = new List<Error>();
                }
                errors.Add(new Error(e.Message));
            }

            return result;
        }

        /// <summary>
        ///     Загрузить конфиг файл
        /// </summary>
        /// <typeparam name="TConf">Тип конфига</typeparam>
        /// <param name="path">Путь к конфигу</param>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Конфиг файл (null в случае ошибки)</returns>
        public TConf Load<TConf>(string path, out List<Error> errors)
            where TConf : class
            => Load(path, typeof(TConf), out errors) as TConf;

        /// <summary>
        ///     Сохранить файл конфигурации (путь по-умолчанию)
        /// </summary>
        /// <typeparam name="TConfig">Тип конфига</typeparam>
        /// <param name="config">Конфиг объект</param>
        public void Save<TConfig>(TConfig config)
            where TConfig : class
        {
            var type = typeof(TConfig);
            var path = type.GetCustomAttribute<ConfigDefaultAttribute>()?.Name;
            if (path == null)
            {
                path = $"{type.Name}.json";
            }
            using (var provider = new FileStockProvider(path))
            {
                var mapper = new JsonMappingProvider(provider);
                mapper.Serialize(config);
            }
        }
        /// <summary>
        ///     Сохранить файл конфигурации
        /// </summary>
        /// <typeparam name="TConfig">Тип конфига</typeparam>
        /// <param name="config">Конфиг объект</param>
        /// <param name="path">Путь для сохранения</param>
        public void Save<TConfig>(TConfig config, string path)
            where TConfig : class
        {
            using (var provider = new FileStockProvider(path))
            {
                var mapper = new JsonMappingProvider(provider);
                mapper.Serialize(config);
            }
        }

        #endregion Public methods

        #region Private methods

        /// <summary>
        ///     Загрузить конфиг файл
        /// </summary>
        private object Load(string path, Type confType, out List<Error> errors)
        {
            errors = null;
            object result = null;
            try
            {
                using (var provider = new FileSourceProvider(path))
                {
                    IMappingProvider mapper = new JsonMappingProvider(provider);
                    result = mapper.Deserialize(confType);
                }
            }
            catch (Exception e)
            {
                errors = new List<Error>();
                errors.Add(new Error(e.Message));
            }

            return result;
        }

        #endregion Private methods
    }
}