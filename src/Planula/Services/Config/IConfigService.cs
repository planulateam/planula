﻿using System;
using System.Collections.Generic;
using System.Text;
using Planula.Models;
using Planula.Models.Config;
using Planula.Providers.Mapping;
using Planula.Providers.Streams;

namespace Planula.Services.Config
{
    /// <summary>
    ///     Сервис конфигурации
    /// </summary>
    public interface IConfigService
    {
        /// <summary>
        ///     Конфигурация приложения
        /// </summary>
        AppConfig Config { get; }

        /// <summary>
        ///     Загрузить все конфиги
        /// </summary>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Результат выполнения</returns>
        bool Load(out List<Error> errors);
        /// <summary>
        ///     Загрузить конфиг файл
        /// </summary>
        /// <typeparam name="TConf">Тип конфига</typeparam>
        /// <param name="path">Путь к конфигу</param>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Конфиг файл (null в случае ошибки)</returns>
        TConf Load<TConf>(string path, out List<Error> errors)
            where TConf : class;
        /// <summary>
        ///     Сохранить файл конфигурации
        /// </summary>
        /// <typeparam name="TConfig">Тип конфига</typeparam>
        /// <param name="config">Конфиг объект</param>
        void Save<TConfig>(TConfig config)
            where TConfig : class;
        /// <summary>
        ///     Сохранить файл конфигурации
        /// </summary>
        /// <typeparam name="TConfig">Тип конфига</typeparam>
        /// <param name="config">Конфиг объект</param>
        /// <param name="path">Путь для сохранения</param>
        void Save<TConfig>(TConfig config, string path)
            where TConfig : class;
    }
}
