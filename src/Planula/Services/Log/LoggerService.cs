﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks.Dataflow;
using Planula.Models;

namespace Planula.Services.Log
{
    /// <summary>
    ///     Сервис логгирования
    /// </summary>
    public class LoggerService : ILoggerService
    {
        #region Public methods

        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Info(string message)
            => Write(message, ELogType.INFO);
        /// <summary>
        ///     Вывести сообщение о предупреждении
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Warn(string message)
            => Write(message, ELogType.WARNING);
        /// <summary>
        ///     Вывести сообщение об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Error(string message)
            => Write(message, ELogType.ERROR);

        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип сообщения</param>
        public void Write(string message, ELogType type)
        {
            // TODO: Добавить вывод отправителя
            Console.WriteLine($"[{type.ToString().ToUpper()}] | [{DateTime.Now.ToShortTimeString()}]: {message}");
        }

        #endregion Public methods
    }
}
