﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks.Dataflow;
using Planula.Models;

namespace Planula.Services.Log
{
    /// <summary>
    ///     Сервис логгирования
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Info(string message);
        /// <summary>
        ///     Вывести сообщение о предупреждении
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Warn(string message);
        /// <summary>
        ///     Вывести сообщение об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Error(string message);
        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип сообщения</param>
        void Write(string message, ELogType type);
    }
}
