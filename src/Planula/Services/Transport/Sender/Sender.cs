﻿using CLI.Models;
using Newtonsoft.Json;
using Planula.Attributes;
using Planula.Models;
using Planula.Models.Config;
using Planula.Services.Config;
using Planula.Services.Transport.Extentions;
using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using Planula.Services.Transport.Models.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Planula.Services.Transport.Sender
{
    public class Sender
    {
        private IConfigService _configService { get; set; }
        private KnownNodesConfig _nodes => _configService.Config.KnownNodes;

        public Sender(IConfigService configService)
        {
            _configService = configService;
        }

        #region public method

        /// <summary>
        ///     Проверяет доступность узла
        /// </summary>
        public async Task<bool> HandshakeAsync(IPAddress ip, ushort port)
        {
            var handshakeMagic = new Random().Next();
            var xPacketOut = XPacketConverter.Serialize(
                XPacketType.Handshake,
                new TPacketHandshake
                {
                    MagicHandshakeNumber = handshakeMagic
                });

            var packetIn = await SendPacketAsync(ip, port, xPacketOut);

            var handshakeIn = XPacketConverter.Deserialize<TPacketHandshake>(packetIn);

            return handshakeIn.MagicHandshakeNumber == handshakeMagic - 15;
        }

        /// <summary>
        ///     Отправляет новые блоки
        /// </summary>
        public async Task<List<Error>> SendNewBlockAsync(Block block)
        {
            var xPacketOut = XPacketConverter.Serialize(
                XPacketType.NewBlock,
                new TPacketBlock
                {
                    Block = JsonConvert.SerializeObject(block)
                });

            var errors = await SendPacketToOtherNodesAsync(_nodes.Nodes, xPacketOut);

            return errors;
        }

        /// <summary>
        ///     Получить все известные узлы
        /// </summary>
        /// <returns></returns>
        public List<Node> GetOtherNodes()
        {
            object locker = new object();

            var knownNodes = _nodes.Nodes;
            var otherNodes = new List<Node>();
            if (knownNodes.Count() < 0)
            {
                return otherNodes;
            }

            var tasks = new List<Task>();

            foreach (var node in knownNodes)
            {
                var t = Task.Run(async () =>
                {
                    try
                    {
                        var incNodes = await GetOtherNodesFromNode(node.Ip, node.Port);
                        if (incNodes != null)
                        {
                            lock (locker)
                            {
                                otherNodes.AddRange(incNodes);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });

                tasks.Add(t);
            }

            Task.WaitAll(tasks.ToArray());

            return otherNodes;
        }

        /// <summary>
        ///     Добавить себя в сеть 
        /// </summary>
        public void AddMeToAllOtherNodes(ushort myPort)
        {
            var knownNodes = _nodes.Nodes;

            foreach (var node in knownNodes)
            {
                var t = Task.Run(async () =>
                {
                    await AddMeToNode(node.Ip, node.Port, myPort);
                });
            }
        }

        /// <summary>
        ///     Получить остальные узлы
        /// </summary>
        private async Task<List<Node>> GetOtherNodesFromNode(IPAddress ip, ushort port)
        {
            var xPacketOut = XPacketConverter.Serialize(XPacketType.GetNodes, new TPacketNodes() { Nodes = "" }); // убрать временный костыль "TPacketNodes"

            var packetIn = await SendPacketAsync(ip, port, xPacketOut);

            var tNodes = XPacketConverter.Deserialize<TPacketNodes>(packetIn);

            var nodes = JsonConvert.DeserializeObject<List<Node>>(tNodes.Nodes);

            return nodes;
        }

        /// <summary>
        ///     Добавить себя узлу в список доступных узлов 
        /// </summary>
        private async Task AddMeToNode(IPAddress ip, ushort port, ushort myPort)
        {
            var xPacketOut = XPacketConverter.Serialize(XPacketType.AddMeAsNode, new TPacketNode {
                Port = myPort
            });

            await SendPacketAsync(ip, port, xPacketOut);
        }


        #endregion

        #region private methods

        private async Task<List<Error>> SendPacketToOtherNodesAsync(List<Node> nodes, XPacket packet)
        {
            var errors = new List<Error>();
            var tasks = new List<Task>();
            var bytes = packet.ToByteArray();

            if (nodes.Count() < 0)
            {
                errors.Add(new Error("Других узлов сети не обнаружено, проверьте конфигурационный файл"));
            }

            foreach (var node in nodes)
            {
                tasks.Add(HandleResponseAsync(SendBytesAsync(node.Ip, node.Port, bytes)));
                //tasks.Last().Start();
            }

            await Task.WhenAll(tasks.ToArray());

            return errors;

            async Task HandleResponseAsync(Task<List<byte>> sendingBytes)
            {
                var response = await sendingBytes;
                var packetIn = XPacketHelper.Parse(response);
                if (packetIn.GetPacketType() == XPacketType.Result)
                {
                    var result = XPacketConverter.Deserialize<TPacketResult>(packetIn);
                    if (!result.IsSuccess)
                        errors.Add(new Error(result.Error));
                }
            }
        }

        public async Task<XPacket> SendPacketAsync(IPAddress ip, ushort port, XPacket packet)
        {
            var bytes = packet.ToByteArray();
            var response = await SendBytesAsync(ip, port, bytes);

            return XPacketHelper.Parse(response);
        }

        private async Task<List<byte>> SendBytesAsync(IPAddress ip, ushort port, byte[] bytes)
        {
            List<byte> bytesIn = null;
            TcpClient client = null;

            try
            {
                client = new TcpClient(ip.ToString(), port);
                NetworkStream stream = client.GetStream();
                await stream.WriteBytesAsync(bytes);

                bytesIn = stream.GetBytes();
            }
            catch (Exception ex)
            {
                Error(ex.Message);
            }
            finally
            {
                if (client != null)
                    client.Close();
            }

            return bytesIn;
        }

        private void Ok(string message)
           => Console.WriteLine(message);

        private void Error(string message)
           => Console.WriteLine(message);

        private void Warning(string message)
           => Console.WriteLine(message);

        #endregion

    }
}
