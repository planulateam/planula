﻿using Planula.Services.Transport.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Helpers
{
    public static class XPacketTypeManager
    {
        public static readonly Dictionary<XPacketType, Tuple<byte, byte>> TypeDictionary = new Dictionary<XPacketType, Tuple<byte, byte>>();

        public static void RegisterTypes()
        {
            RegisterType(XPacketType.Handshake, 1, 1);
            RegisterType(XPacketType.NewBlock, 2, 1);
            RegisterType(XPacketType.AllBlocks, 2, 2);
            RegisterType(XPacketType.Result, 3, 1);
            RegisterType(XPacketType.Test, 4, 1);
            RegisterType(XPacketType.AddMeAsNode, 5, 1);
            RegisterType(XPacketType.FogetMe, 5, 2);
            RegisterType(XPacketType.GetNodes, 6, 1);

        }

        public static void RegisterType(XPacketType type, byte btype, byte bsubtype)
        {
            if (TypeDictionary.ContainsKey(type))
            {
                throw new Exception($"Packet type {type:G} is already registered.");
            }

            TypeDictionary.Add(type, Tuple.Create(btype, bsubtype));
        }
    }
}
