﻿using Planula.Services.Transport.Extentions;
using Planula.Services.Transport.Models;
using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Planula.Services.Transport.Helpers
{
    public static class XPacketConverter
    {
        private static List<Tuple<PropertyInfo, byte>> GetProperties(Type t)
        {
            return t.GetProperties(BindingFlags.Instance |
                               BindingFlags.NonPublic |
                               BindingFlags.Public)
            .Where(field => field.GetCustomAttribute<XFieldAttribute>() != null)
            .Select(field => Tuple.Create(field, field.GetCustomAttribute<XFieldAttribute>().FieldID))
            .ToList();
        }

        public static XPacket Serialize(XPacketType type, object obj)
        {
            var properties = GetProperties(obj.GetType());
            
            var usedUp = new List<byte>();

            foreach (var property in properties)
            {
                if (usedUp.Contains(property.Item2))
                {
                    throw new Exception("One field used two times.");
                }

                usedUp.Add(property.Item2);
            }

            var packet = new XPacket(type);

            foreach (var property in properties)
            {
                packet.SetValue(property.Item2, property.Item1.GetValue(obj));
            }

            return packet;
        }

        public static T Deserialize<T>(XPacket packet, bool strict = false) where T : class, new()
        {
            var fields = GetProperties(typeof(T));
            var instance = Activator.CreateInstance<T>();
            var obj = new T();


            if (fields.Count == 0)
            {
                return instance;
            }

            foreach (var tuple in fields)
            {
                var field = tuple.Item1;
                var packetFieldId = tuple.Item2;

                if (!packet.HasField(packetFieldId))
                {
                    if (strict)
                    {
                        throw new Exception($"Couldn't get field[{packetFieldId}] for {field.Name}");
                    }

                    continue;
                }

                var method = typeof(XPacket)
                    .GetMethod("GetValue");

                var value = typeof(XPacket)
                    .GetMethod("GetValue")?
                    .MakeGenericMethod(field.PropertyType)
                    .Invoke(packet, new object[] { packetFieldId });

                if (value == null)
                {
                    if (strict)
                    {
                        throw new Exception($"Couldn't get value for field[{packetFieldId}] for {field.Name}");
                    }

                    continue;
                }

                field.SetValue(instance, value);
            }

            return instance;
        }
    }
}
