﻿using Planula.Services.Transport.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Planula.Services.Transport.Helpers
{
    public static class XPacketHelper
    {
        public static readonly int SizeOfStart = 3;
        public static readonly int SizeOfType = 2;
        public static readonly int SizeOfEnd= 2;

        public static readonly int SizeOfNumber = 1;
        public static readonly int SizeOfSize = 4;


        public static XPacket Parse(List<byte> packet)
        {
            if (packet.Count < SizeOfStart + SizeOfType + SizeOfEnd)
            {
                return null;
            }

            if (packet[0] != 0xAF ||
                packet[1] != 0xAA ||
                packet[2] != 0xAF)
            {
                return null;
            }

            var mIndex = packet.Count - 1;

            if (packet[mIndex - 1] != 0xFF ||
                packet[mIndex] != 0x00)
            {
                return null;
            }

            var type = packet[3];
            var subtype = packet[4];

            var xpacket = new XPacket(type, subtype);
            var fields = packet.Skip(SizeOfStart + SizeOfType).ToArray();

            while (true)
            {
                if (fields.Length == 2)
                {
                    return xpacket;
                }

                var id = fields[0];
                var size = BitConverter.ToInt32(fields, 1);

                var contents = size != 0 ?
                fields.Skip(SizeOfNumber + SizeOfSize).Take(size).ToArray() : null;

                xpacket.Fields.Add(new XPacketField
                {
                    FieldID = id,
                    FieldSize = size,
                    Contents = contents
                });

                fields = fields.Skip(SizeOfNumber + SizeOfSize + size).ToArray();
            }
        }
    }
}
