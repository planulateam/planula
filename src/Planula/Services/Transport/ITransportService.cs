﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Planula.Models;

namespace Planula.Services.Transport
{
    /// <summary>
    ///     Интерфейс транспортного сервиса
    /// </summary>
    public interface ITransportService
    {
        /// <summary>
        ///     Работает ли транспортный сервис
        /// </summary>
        bool IsWorking { get; }

        /// <summary>
        ///     Отправить блок транзакции. 
        ///     В случае успеха вернуть true,
        ///     в случае ошибки, вернуть false и заполнить переменную errors
        /// </summary>
        /// <param name="block">Блок транзакции</param>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Результат выполнения</returns> 
        Task<(bool, List<Error>)> SendAsync(Block block);

        /// <summary>
        ///     Запустить принятие новых блоков.
        ///     Запускается в начеле исполнения, либо после перезагрузки.
        /// </summary>
        /// <param name="handler">Обработчик пришедших блоков транзакций</param>
        /// <returns>Результат запуска</returns>
        bool Listen(Func<Block, bool> handler, ushort port);

        /// <summary>
        ///     Остановить принятие новых блоков
        /// </summary>
        /// <returns>Результат выполнения</returns>
        bool Stop();
    }
}
