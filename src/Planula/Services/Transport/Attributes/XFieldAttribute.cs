﻿using System;

namespace Planula.Services.Transport.Models.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class XFieldAttribute : Attribute
    {
        public byte FieldID { get; }

        public XFieldAttribute(byte fieldId)
        {
            FieldID = fieldId;
        }
    }
}
