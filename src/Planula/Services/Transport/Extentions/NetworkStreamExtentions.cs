﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Planula.Services.Transport.Extentions
{
    public static class NetworkStreamExtentions
    {
        /// <summary>
        ///     Получить все байты из потока списком
        /// </summary>
        public static List<byte> GetBytes(this NetworkStream stream)
        {
            var listBytes = new List<byte>();
            byte[] buff = new byte[256];
            do
            {
                var count = stream.Read(buff, 0, buff.Length);
                listBytes.AddRange(buff.Take(count));
            } while (stream.DataAvailable);

            return listBytes.DeleteExtraZeroBytes();
        }

        /// <summary>
        ///     Записать байты в поток из списка
        /// </summary>
        public static void WriteBytes(this NetworkStream stream, List<byte> bytes)
        {
            stream.Write(bytes.ToArray(), 0, bytes.Count);
        }

        /// <summary>
        ///     Записать байты в поток асинхроно из списка
        /// </summary>
        public static Task WriteBytesAsync(this NetworkStream stream, List<byte> bytes)
        {
            return stream.WriteAsync(bytes.ToArray(), 0, bytes.Count);
        }

        /// <summary>
        ///     Записать байты в поток асинхроно из массива
        /// </summary>
        public static Task WriteBytesAsync(this NetworkStream stream, byte[] bytes)
        {
            return stream.WriteAsync(bytes, 0, bytes.Length);
        }
    }
}
