﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Planula.Services.Transport.Extentions
{
    public static class ByteArrayExtentions
    {
        /// <summary>
        ///     Получить обьект из массива байт
        /// </summary>
        public static T ByteArrayToFixedObject<T>(this byte[] bytes)
        {
            T structure = default(T);

            if (typeof(T) == typeof(string))
            {
                structure = (T)(object)Encoding.UTF8.GetString(bytes);
            }
            else
            {
                var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);

                try
                {
                    structure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
                }
                finally
                {
                    handle.Free();
                }
            }

            

            return structure;
        }
    }
}
