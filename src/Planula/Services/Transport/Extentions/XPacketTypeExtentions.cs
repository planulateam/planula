﻿using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Extentions
{
    public static class XPacketTypeExtentions
    {
        /// <summary>
        ///     Получить байты
        /// </summary>
        public static Tuple<byte, byte> GetBytes(this XPacketType type)
        {
            if (!XPacketTypeManager.TypeDictionary.ContainsKey(type))
            {
                throw new Exception($"Packet type {type:G} is not registered.");
            }

            return XPacketTypeManager.TypeDictionary[type];
        }
    }
}
