﻿using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Planula.Services.Transport.Extentions
{
    public static class XPacketExtentions
    {
        /// <summary>
        ///     Получить тип пакета
        /// </summary>
        public static XPacketType GetPacketType(this XPacket packet)
        {
            var type = packet.PacketType;
            var subtype = packet.PacketSubtype;

            foreach (var tuple in XPacketTypeManager.TypeDictionary)
            {
                var value = tuple.Value;

                if (value.Item1 == type && value.Item2 == subtype)
                {
                    return tuple.Key;
                }
            }

            return XPacketType.Unknown;
        }

        /// <summary>
        ///     Возвращает пакет в виде массива байт
        /// </summary>
        public static byte[] ToByteArray(this XPacket packet)
        {
            var byteArray = new MemoryStream();

            byteArray.Write(
            new byte[] { 0xAF, 0xAA, 0xAF, packet.PacketType, packet.PacketSubtype }, 0, XPacketHelper.SizeOfStart + XPacketHelper.SizeOfType);

            var fields = packet.Fields.OrderBy(field => field.FieldID);

            foreach (var field in fields)
            {
                byteArray.Write(new[] { field.FieldID }.Concat(BitConverter.GetBytes(field.FieldSize)).ToArray(), 0, XPacketHelper.SizeOfNumber + XPacketHelper.SizeOfSize);
                byteArray.Write(field.Contents, 0, field.Contents.Length);
            }

            byteArray.Write(new byte[] { 0xFF, 0x00 }, 0, XPacketHelper.SizeOfEnd);

            return byteArray.ToArray();
        }

        /// <summary>
        ///     Возвращает обьект в виде массива байт
        /// </summary>
        public static byte[] FixedObjectToByteArray(this XPacket packet, object value)
        {
            byte[] rawdata = null;
            if (value.GetType() == typeof(string))
            {
                var str = value.ToString();
                rawdata = Encoding.UTF8.GetBytes(str);
            }
            else
            {
                var rawsize = Marshal.SizeOf(value);
                rawdata = new byte[rawsize];
                var handle = GCHandle.Alloc(rawdata,
                 GCHandleType.Pinned);

                Marshal.StructureToPtr(value,
                    handle.AddrOfPinnedObject(),
                    false);

                handle.Free();
            }           


            return rawdata;
        }

        
        public static XPacketField GetField(this XPacket packet, byte id)
        {
            foreach (var field in packet.Fields)
            {
                if (field.FieldID == id)
                {
                    return field;
                }
            }

            return null;
        }

        public static bool HasField(this XPacket packet, byte id)
        {
            return packet.GetField(id) != null;
        }

        public static T GetValue<T>(this XPacket packet, byte id) where T : struct
        {
            var field = packet.GetField(id);

            if (field == null)
            {
                throw new Exception($"Field with ID {id} wasn't found.");
            }

            return field.Contents.ByteArrayToFixedObject<T>();
        }

        public static void SetValue(this XPacket packet, byte id, object structure)
        {
            if (!structure.GetType().IsValueType && structure.GetType() != typeof(string))
            {
                throw new Exception("Only value types are available.");
            }

            var field = packet.GetField(id);

            if (field == null)
            {
                field = new XPacketField
                {
                    FieldID = id
                };

                packet.Fields.Add(field);
            }

            var bytes = packet.FixedObjectToByteArray(structure);

            if (bytes.Length > int.MaxValue)
            {
                throw new Exception($"Object is too big. Max length is {int.MaxValue} bytes.");
            }

            field.FieldSize = bytes.Length;
            field.Contents = bytes;
        }
    }
}
