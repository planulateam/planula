﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace Planula.Services.Transport.Extentions
{
    public static class ByteListExtentions
    {
        /// <summary>
        ///     Получить список в конце которого будет только 1 нулевой байт
        /// </summary>
        public static List<byte> DeleteExtraZeroBytes(this List<byte> bytes)
        {
            var copyBytes = bytes.Select(item => item).ToList();

            while (true)
            {
                if (copyBytes.Count == 0)
                    break;

                if (copyBytes.Last() == 0x00)
                    copyBytes.RemoveAt(copyBytes.Count - 1);
                else break;
            }
            copyBytes.Add(0x00);

            return copyBytes;
        }
    }
}
