﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models
{ 
    public class XPacketField
    {
        public byte FieldID { get; set; }
        public int FieldSize { get; set; }
        public byte[] Contents { get; set; }
    }
}
