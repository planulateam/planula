﻿using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    /// <summary>
    ///    Обьект для рукопожатия (установление соединения с сервером)
    /// </summary>
    public class TPacketHandshake
    {
        [XField(1)]
        public int MagicHandshakeNumber { get; set; }
    }
}
