﻿using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    public class TPacketResult
    {
        [XField(1)]
        public bool IsSuccess{ get; set; }

        [XField(2)]
        public string Error { get; set; }
    }
}
