﻿using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    public class TPacketNode
    {
        [XField(1)]
        public ushort Port { get; set; }
    }
}
