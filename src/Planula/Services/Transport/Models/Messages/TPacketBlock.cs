﻿using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    /// <summary>
    ///     Обьект для передачи блоков
    /// </summary>
    public class TPacketBlock
    {
        [XField(1)]
        public string Block  { get; set; }
    }
}
