﻿using Planula.Services.Transport.Models;
using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    // ReSharper disable once InconsistentNaming
    public class TPacketTest
    {
        [XField(0)]
        public int TestInt { get; set; }

        [XField(1)]
        public double TestDouble { get; set; }

        [XField(2)]
        public bool TestBoolean { get; set; }

        [XField(3)]
        public string TestString { get; set; }
    }
}
