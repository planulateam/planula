﻿using Planula.Services.Transport.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models.Messages
{
    public class TPacketNodes
    {
        [XField(0)]
        public string Nodes { get; set; }
    }
}
