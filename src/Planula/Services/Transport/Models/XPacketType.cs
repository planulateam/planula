﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.Transport.Models
{
    public enum XPacketType
    {
        Unknown,
        Handshake,
        NewBlock,
        AllBlocks,
        Result,
        Test,
        AddMeAsNode,
        FogetMe,
        GetNodes
    }
}
