﻿using Planula.Services.Transport.Extentions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace Planula.Services.Transport.Models
{
    public class XPacket
    {
        public byte PacketType { get; private set; }
        public byte PacketSubtype { get; private set; }
        public List<XPacketField> Fields { get; set; } = new List<XPacketField>();

        private XPacket() { }
        public XPacket(byte type, byte subtype)
        {
            PacketType = type;
            PacketSubtype = subtype;
        }
        public XPacket(XPacketType type)
        {
            PacketType = type.GetBytes().Item1;
            PacketSubtype = type.GetBytes().Item2;
        }

        public T GetValue<T>(byte id)
        {
            var field = this.GetField(id);

            if (field == null)
            {
                throw new Exception($"Field with ID {id} wasn't found.");
            }

            return field.Contents.ByteArrayToFixedObject<T>();
        }
    }
}
