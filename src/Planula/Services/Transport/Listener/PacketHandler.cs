﻿using Newtonsoft.Json;
using Planula.Models;
using Planula.Models.Config;
using Planula.Services.Config;
using Planula.Services.Transport.Extentions;
using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using Planula.Services.Transport.Models.Messages;
using System;
using System.Net;
using System.Net.Sockets;

namespace Planula.Services.Transport.Listener
{
    public class PacketHandler
    {
        private IConfigService _configService { get; set; }
        private KnownNodesConfig _nodes => _configService.Config.KnownNodes;

        private Func<Block, bool> _HandlerNewBlock;

        public PacketHandler(IConfigService configService, Func<Block, bool> handlerNewBlock)
        {
            _configService = configService;
            _HandlerNewBlock = handlerNewBlock;
        }

        /// <summary>
        /// Обработка пакета
        /// </summary>
        public void Procces(TcpClient client, NetworkStream stream, XPacket packet)
        {
            var packetType = packet.GetPacketType();
            if (packetType == XPacketType.Handshake)
            {
                ProcessHandshake(stream, packet);
            }

            if (packetType == XPacketType.NewBlock)
            {
                ProcessNewBlock(stream, packet);
            }

            if (packetType == XPacketType.Test)
            {
                ProcessPacketTest(stream, packet);
            }

            if (packetType == XPacketType.GetNodes)
            {
                ProcessPacketGetNodes(stream, packet);
            }

            if (packetType == XPacketType.AddMeAsNode)
            {
                ProcessPacketAddMeAsNode(client, stream, packet);
            }

        }

        /// <summary>
        ///     Обработка рукопожатия
        /// </summary>
        private void ProcessHandshake(NetworkStream stream, XPacket packetIn)
        {
            var number = 15;
            var handshake = XPacketConverter.Deserialize<TPacketHandshake>(packetIn);

            handshake.MagicHandshakeNumber -= number;

            var packetOut = XPacketConverter.Serialize(XPacketType.Handshake, handshake)
                    .ToByteArray();
            stream.Write(packetOut);
        }

        /// <summary>
        ///     Обработка нового блока
        /// </summary>
        private void ProcessNewBlock(NetworkStream stream, XPacket packetIn)
        {
            var packetBlock = XPacketConverter.Deserialize<TPacketBlock>(packetIn);
            var block = JsonConvert.DeserializeObject<Block>(packetBlock.Block);

            var isValidated = false;

            if (_HandlerNewBlock != null)
                isValidated = _HandlerNewBlock(block);

            TPacketResult result = null;
            if (isValidated)
            {
                result = new TPacketResult
                {
                    IsSuccess = true,
                    Error = "123" // TODO: Добавить обработку null
                };
            }
            else
            {
                result = new TPacketResult
                {
                    IsSuccess = false,
                    Error = "Block is not valid"
                };
            }

            var packetOut = XPacketConverter.Serialize(XPacketType.Result, result).ToByteArray();
            stream.Write(packetOut);
        }

        /// <summary>
        ///     Обработка тестового пакета
        /// </summary>
        private void ProcessPacketTest(NetworkStream stream, XPacket packetIn)
        {
            var packetTest = XPacketConverter.Deserialize<TPacketTest>(packetIn);

            var xPacketOut = XPacketConverter.Serialize(
                XPacketType.Test,
                new TPacketTest
                {
                    TestInt = packetTest.TestInt,
                    TestDouble = packetTest.TestDouble,
                    TestBoolean = packetTest.TestBoolean,
                    TestString = packetTest.TestString
                }).ToByteArray();

            stream.Write(xPacketOut);
        }

        /// <summary>
        ///     Обработка тестового пакета
        /// </summary>
        private void ProcessPacketGetNodes(NetworkStream stream, XPacket packetIn)
        {
            var nodes = new TPacketNodes
            {
                Nodes = JsonConvert.SerializeObject(_configService.Config.KnownNodes.Nodes)
            };

            var xPacketOut = XPacketConverter.Serialize(XPacketType.GetNodes, nodes).ToByteArray();

            stream.Write(xPacketOut);
        }

        /// <summary>
        ///     Обработка тестового пакета
        /// </summary>
        private void ProcessPacketAddMeAsNode(TcpClient client, NetworkStream stream, XPacket packetIn)
        {
            var node = XPacketConverter.Deserialize<TPacketNode>(packetIn);

            var ip = ((IPEndPoint)client.Client.RemoteEndPoint).Address;
            var port = node.Port;

            if (!_configService.Config.KnownNodes.Nodes.Exists(_ => _.Ip.Equals(ip) && _.Port == port))
            {
                _configService.Config.KnownNodes.Nodes.Add(new Node
                {
                    Ip = ip,
                    Port = port
                });
                _configService.Save(_configService.Config.KnownNodes);
            }

            TPacketResult result = new TPacketResult
                {
                    IsSuccess = true,
                    Error = "123" // TODO: Добавить обработку null
            };

            var packetOut = XPacketConverter.Serialize(XPacketType.Result, result).ToByteArray();
            stream.Write(packetOut);
        }
    }
}
