﻿using Planula.Models;
using Planula.Services.Config;
using Planula.Services.Transport.Extentions;
using Planula.Services.Transport.Helpers;
using Planula.Services.Transport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Planula.Services.Transport.Listener
{
    public class Listener
    {
        private IConfigService _configService { get; set; }
        private CancellationTokenSource _tokenSource = new CancellationTokenSource();
        private Func<Block, bool> _HandlerNewBlock;
        private ushort _port;

        public ushort Port { get => _port; }

        public bool IsListening { get; set; }

        public Listener(IConfigService configService, ushort port)
        {
            _configService = configService;
            _port = port;
        }


        public void StartListenAsync(Func<Block, bool> handlerNewBlock)
        {
            _HandlerNewBlock = handlerNewBlock;
            var task = new Task(() => StartListen(), _tokenSource.Token);
            task.Start();

            
        }

        public bool StopListen()
        {
            _tokenSource.Cancel();
            if (IsListening)
                Console.WriteLine($"Сервер остановлен.");
            IsListening = false;
            return true;
        }

        private void StartListen()
        {

            var localAddr = IPAddress.Parse("0.0.0.0");
            var listener = new TcpListener(localAddr, _port);
            listener.Start();

            try
            {
                var port = ((IPEndPoint)listener.LocalEndpoint).Port;
                IsListening = true;


                Console.WriteLine($"Сервер запущен. {localAddr}:{port}");

                while (true)
                {
                    HandleAcception(listener);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("StartListen Error:" + ex.Message);
                IsListening = false;
            }
            finally
            {
                if (listener != null)
                    listener.Stop();
            }
        }

        private void HandleAcception(TcpListener listener)
        {
            // получаем входящее подключение
            TcpClient client = listener.AcceptTcpClient();
            Console.WriteLine("Подключен клиент. Выполнение запроса...");

            // получаем сетевой поток для чтения и записи
            NetworkStream stream = client.GetStream();
            var handler = new PacketHandler(_configService, _HandlerNewBlock);

            try
            {
                var packet = XPacketHelper.Parse(stream.GetBytes());
                handler.Procces(client, stream, packet);
            }
            catch (Exception ex)
            {
                Console.WriteLine("HandleAcception Error: " + ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (client != null)
                    client.Close();
            }
        }


    }
}
