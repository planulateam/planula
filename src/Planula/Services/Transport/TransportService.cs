﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLI.Models;
using Planula.Models;
using Planula.Services.Config;

namespace Planula.Services.Transport
{
    public class TransportService : ITransportService
    {
        public TransportService(IConfigService configService)
        {
            _configService = configService;
            _sender = new Sender.Sender(configService);
        }

        #region Public methods

        #region Implementation ITransportService

        /// <summary>
        ///     Работает ли транспортный сервис
        /// </summary>
        public bool IsWorking => _listener != null && _listener.IsListening;

        /// <summary>
        ///     Отправить блок транзакции. 
        ///     В случае успеха вернуть true,
        ///     в случае ошибки, вернуть false и заполнить переменную errors
        /// </summary>
        /// <param name="block">Блок транзакции</param>
        /// <param name="errors">Список ошибок</param>
        /// <returns>Результат выполнения</returns>
        public async Task<(bool, List<Error>)> SendAsync(Block block)
        {
            var errors = await _sender.SendNewBlockAsync(block);
            return (errors.Count > 0,  errors);
        }

        /// <summary>
        ///     Запустить принятие новых блоков.
        ///     Запускается в начеле исполнения, либо после перезагрузки.
        /// </summary>
        /// <param name="handler">Обработчик пришедших блоков транзакций</param>
        /// <returns>Результат запуска</returns>
        public bool Listen(Func<Block, bool> handler, ushort port)
        {
            try
            {
                if (_listener != null)
                    _listener.StopListen();

                _listener = new Listener.Listener(_configService, port);
                _listener.StartListenAsync(handler);

                var sender = new Sender.Sender(_configService);
                var otherNodes = sender.GetOtherNodes();

                _configService.Config.KnownNodes.Nodes.AddRange(otherNodes);

                _configService.Config.KnownNodes.Nodes = _configService.Config.KnownNodes.Nodes
                    .Select(_ => new { _.IpAddress, _.Port })
                    .Distinct()
                    .Select(_ => new Node { IpAddress = _.IpAddress, Port = _.Port })
                    .ToList();

                _configService.Save(_configService.Config.KnownNodes);

                sender.AddMeToAllOtherNodes(_listener.Port);

                return true;
            }
            catch (Exception e)
            {
                Error("TransportService Error:" + e.Message);

                if (_listener != null)
                {
                    _listener.StopListen();
                    _listener = null;
                }

                return false;
            }
        }

        /// <summary>
        ///     Остановить принятие новых блоков
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool Stop()
        {
            return _listener.StopListen();
        }

        #endregion Implementation ITransportService

        #endregion Public methods

        #region Private methods

        private void Ok(string message)
           => Console.WriteLine(message);

        private void Error(string message)
           => Console.WriteLine(message);

        private void Warning(string message)
           => Console.WriteLine(message);

        #endregion

        #region Private fields

        private Listener.Listener _listener;
        private Sender.Sender _sender;
        private IConfigService _configService;

        #endregion  Private fields
    }
}
