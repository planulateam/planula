﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using CLI.Attributes;
using CLI.Helpers;
using CLI.Models;
using Planula.Models;
using Planula.Models.Config;
using Planula.Providers.Mapping;
using Planula.Providers.Streams;
using Planula.Services.Config;
using Planula.Services.Crypto;
using Planula.Services.DB;
using Planula.Services.Log;
using Planula.Services.Miner;
using Planula.Services.Transport;

namespace Planula.Services.Manager
{
    /// <summary>
    ///     Сервис управления сервисами
    /// </summary>
    public class ManagerService
    {
        #region .Ctor
        public ManagerService(
            IMinerService minerService,
            ICryptoService cryptoService,
            ITransportService transportService,
            IConfigService configService,
            IDBService DBService,
            ILoggerService loggerService
        ) {
            _MinerService = minerService;
            _CryptoService = cryptoService;
            _TransportService = transportService;
            _ConfigService = configService;
            _DBService = DBService;
            _LoggerService = loggerService;
        }

        #endregion .Ctor

        #region Public properties

        /// <summary>
        ///     Запущены ли сервисы
        /// </summary>
        public bool IsWorking => _TransportService.IsWorking;

        #endregion Public properties

        #region Public mehtods

        /// <summary>
        ///     Перезапустить систему
        /// </summary>
        [CLIMehod("restart")]
        [CLIDescription("Перезапустить систему")]
        public CLICommandResult Restart()
        {
            if (IsWorking)
            {
                Stop();
            }
            else
            {
                _LoggerService.Warn("Сервис не был запущен");
            }
            Start();

            return Ok(SUCCESS_RESTART_MESS);
        }
        /// <summary>
        ///     Запустить систему
        /// </summary>
        [CLIMehod("start")]
        [CLIDescription("Запустить систему")]
        public CLICommandResult Start(
            [CLIArgument("port", "p")]
            [CLIDescription("Порт подключения")]
            string portStr = "1234")
        {
            try
            {
                LoadConfigs();
                _LoggerService.Info("Загружена конфигурация");
                var port = ushort.Parse(portStr);
                // TODO: Выполнить рестарт системы
                // TODO: Загрузить все конфиги
                var result = _TransportService.Listen((b) =>
                {
                    var isVerified = _MinerService.CheckBlock(b, out var errors);
                    if (isVerified)
                    {
                        _DBService.Blocks.Add(b);
                    }
                    return isVerified;
                }, port);
                if (!result)
                {
                    return Error("Ошибка запуска сервиса");
                }

                return Ok(SUCCESS_RESTART_MESS);
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }
        }
        /// <summary>
        ///     Остановить систему
        /// </summary>
        [CLIMehod("stop")]
        [CLIDescription("Остановить систему")]
        public CLICommandResult Stop()
        {
            // TODO: Выполнить рестарт системы
            // TODO: Загрузить все конфиги

            if (!_TransportService.Stop())
            {
                return Error("Ошибка остановки сервиса");
            }
            return Ok(SUCCESS_RESTART_MESS);
        }
        /// <summary>
        ///     Отобразить блоки
        /// </summary>
        [CLIMehod("getBlocks")]
        [CLIDescription("Отобразить блоки")]
        public CLICommandResult GetBlocks()
        {
            try
            {
                _DBService.Blocks.ForEach(b =>
                {
                    var bStr = $"{{ hash: {b.Hash} }}\n";
                    _LoggerService.Info(bStr);
                });
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }
            return Ok(SUCCESS_GET_BLOCKS_MESS);
        }
        /// <summary>
        ///     Загрузить конфиги
        /// </summary>
        [CLIMehod("loadConfigs")]
        [CLIDescription("Загрузить конфиг файлы")]
        public CLICommandResult LoadConfigs()
        {
            if (!_ConfigService.Load(out var errors))
            {
                return Error(errors.First().Message);
            }

            return Ok(SUCCESS_LOAD_CONFIGS_MESS);
        }

        /// <summary>
        ///     Создать новый блок транзакции
        /// </summary>
        /// <returns>Блок данных типа <see cref="Block"/></returns>
        [CLIMehod("createArticle")]
        [CLIDescription("Создать новую запись / статью")]
        public CLICommandResult CreateArticle(
            [CLIArgument("path", "p")]
            [CLIDescription("Путь к файлу со статьей")]
            string path = "article.txt"
        )
        {
            if (!IsWorking)
            {
                return Error(IS_NOT_WORKING_MESS);
            }

            // Подготавливаем контент
            try
            {
                string content;
                using (var provider = new FileSourceProvider(path))
                {
                    content = Convert.ToBase64String(provider.GetBytes());
                }
                var contentBlock = new ContentBlock
                {
                    Content = content,
                    Created = DateTime.Now
                };

                // Майним блок
                var block = _MinerService.CreateNewBlock(contentBlock);
                var (result, errors) = _TransportService.SendAsync(block).Result;

                if (!result)
                {
                    // TODO: Обдумать логику
                    _DBService.Blocks.Add(block);
                }
                else
                {
                    // TODO: выводить все сообщения
                    return Error(errors.FirstOrDefault()?.Message);
                }
            }
            catch(Exception e)
            {
                return Error(e.Message);
            }

            return Ok(SUCCESS_CREATE_ARTICLE_MESS);
        }
        /// <summary>
        ///     Создать новую пару ключей
        /// </summary>
        [CLIMehod("createKeys")]
        [CLIDescription("Создать новую пару ключей")]
        public CLICommandResult CreateKeys(
            [CLIArgument("path", "p")]
            [CLIDescription("Наименование для файла с ключами")]
            string fileName = "keys.json"
        )
        {
            var (priv, pub) = _CryptoService.CreateKeys();
            var conf = new CryptoConfig
            {
                PrivateKey = priv,
                PublicKey = pub,
            };

            _ConfigService.Save(conf, fileName);

            return Ok(SUCCESS_CREATE_KEYS_MESS(fileName));
        }

        /// <summary>
        ///     Добавление нового узла
        /// </summary>
        [CLIMehod("addNode")]
        [CLIDescription("Добавление нового узла")]
        public CLICommandResult addNode(
            [CLIArgument("node", "n")]
            [CLIDescription("Ip адрес новой узла")]
            string nodeStr
        )
        {
            try
            {
                var ss = nodeStr.Split(":");
                var ipStr = ss[0];
                var portStr = ss[1];
                ushort.TryParse(portStr, out var port);
                IPAddress.TryParse(ipStr, out var ip);
                if (ip == null || port == default)
                {
                    return Error($"Неверный формат аргумента: {nodeStr}");
                }
                var node = new Node
                {
                    Ip = ip,
                    Port = port
                };

                var conf = _ConfigService.Config.KnownNodes;
                if (conf == null)
                {
                    conf = new KnownNodesConfig()
                    {
                        Nodes = new List<Node>(),
                    };
                }
                conf.Nodes.Add(node);

                _ConfigService.Save(conf);
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }

            return Ok(SUCCESS_ADD_NODE_MESS(nodeStr));
        }

        #endregion Public mehtods

        #region Private methods

        private CLICommandResult Ok(string message)
            => new CLICommandResult
            {
                Message = message,
                Type = ECLIResultType.SUCCESS,
            };

        private CLICommandResult Error(string message)
            => new CLICommandResult
            {
                Message = message,
                Type = ECLIResultType.ERROR,
            };

        #endregion Private methods

        #region Consts

        private const string SUCCESS_CREATE_ARTICLE_MESS = "Успешное создание статьи";
        private const string SUCCESS_RESTART_MESS = "Успешный рестарт системы";
        private const string SUCCESS_GET_BLOCKS_MESS = "Успешный отображение";
        private const string SUCCESS_LOAD_CONFIGS_MESS = "Успешная загрузка конфиг-файлов";
        private static string SUCCESS_CREATE_KEYS_MESS(string path) => $"Успешное создание ключей по адресу \"{path}\"";
        private static string SUCCESS_ADD_NODE_MESS(string node) => $"Успешное добавление узла: \"{node}\"";

        private const string IS_NOT_WORKING_MESS = "Сервисы не запущены";
        private const string IS_WORKING_ALREADY_MESS = "Сервисы не запущены";

        #endregion Consts

        #region Private fields

        private readonly IMinerService _MinerService;
        private readonly ICryptoService _CryptoService;
        private readonly ITransportService _TransportService;
        private readonly IConfigService _ConfigService;
        private readonly IDBService _DBService;
        private readonly ILoggerService _LoggerService;

        #endregion Private fields
    }
}
