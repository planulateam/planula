﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Planula.Models;
using Planula.Providers.Mapping;
using Planula.Providers.Streams;
using Planula.Services.Config;
using Planula.Services.Crypto;
using Planula.Services.DB;

namespace Planula.Services.Miner
{
    /// <summary>
    ///     Сервис майнинга блоков
    /// </summary>
    public class MinerService : IMinerService
    {
        #region .Ctor

        public MinerService(
            IDBService dbService,
            ICryptoService cryptoService,
            IConfigService configService
        ) {
            _DBService = dbService;
            _CryptoService = cryptoService;
            _ConfigService = configService;
        }

        #endregion .Ctor

        #region Public methods

        #region Implementation IMinerService

        /// <summary>
        ///     Создать новый блок транзакции
        /// </summary>
        /// <returns>Блок данных типа <see cref="Block"/></returns>
        public Block CreateNewBlock(ContentBlock content)
        {
            // Преобразуем в массив байт
            var contentData = GetBytes(content);
            // Вытаскиваем ключи из конфига
            var cryptoConf = _ConfigService.Config.Crypto;
            var privateKey = cryptoConf.PrivateKey;
            var publicKey = cryptoConf.PublicKey;
            // Сериализуем публичный ключ в Base64 формат
            var author = Convert.ToBase64String(GetBytes(publicKey));
            // TODO: Вытаскивать из бд
            var lastHash = _DBService.Blocks.FirstOrDefault()?.Hash ?? "";
            var lastHashData = Convert.FromBase64String(lastHash);

            // Смайнить хеш и соль
            var (hashData, saltData) = Mine(contentData, lastHashData);
            var salt = Convert.ToBase64String(saltData);
            var hash = Convert.ToBase64String(hashData);
            // Вычисляем подпись
            var signData = _CryptoService.ComputeSign(contentData, privateKey);
            var sign = Convert.ToBase64String(signData);

            var block = new Block
            {
                Content = content,
                Author = author,
                Hash = hash,
                Salt = salt,
                Sign = sign,
            };

            return block;
        }
        /// <summary>
        ///     Проверить блок на валидность
        /// </summary>
        /// <param name="block">Блок данных</param>
        /// <param name="errors">Список ошибок, в случае провальной проверки валидации</param>
        /// <returns>Результат проверки</returns>
        public bool CheckBlock(Block block, out List<Error> errors)
        {
            errors = new List<Error>();
            bool result = true;

            try
            {
                // Проверяем хеши на совпадение

                var lastHash = _DBService.Blocks.FirstOrDefault()?.Hash ?? "";
                // Собираем в массив данные для подсчета хеша
                var data = GetDataForHash(block.Content, lastHash, block.Salt);
                // Вычисляем хеш
                var hashData = _CryptoService.ComputeHash(data);
                var hash = Convert.ToBase64String(hashData);
                // Сравниваем результаты
                if (block.Hash != hash)
                {
                    errors.Add(new Error("Hashes are not equals"));
                    result = false;
                }

                // Проверяем подпись

                var contentData = GetBytes(block.Content);
                var signData = Convert.FromBase64String(block.Sign);
                // Вытаскиваем публичный ключ
                var authorData = Convert.FromBase64String(block.Author);
                RSAParameters publicKey;
                using (var provider = new MemorySourceProvider(authorData))
                {
                    IMappingProvider mapper = new JsonMappingProvider(provider);
                    publicKey = mapper.Deserialize<RSAParameters>();
                }
                if (!_CryptoService.CheckSign(contentData, signData, publicKey))
                {
                    errors.Add(new Error("Sign is not valid"));
                    result = false;
                }
            }
            catch (Exception e)
            {
                errors.Add(new Error(e.Message));
            }

            return result;
        }

        #endregion Implementation IMinerService

        #endregion Public methods

        #region Private methods

        /// <summary>
        ///     Проверить хеш на удовлетворение паттерну
        /// </summary>
        /// <param name="hash">Хеш в формате Base64</param>
        /// <returns>Результат проверки</returns>
        private bool CheckHashForPattern(string hash)
            => hash.StartsWith(PATTERN);
        /// <summary>
        ///     Десериализовать в Json и получить массив байт
        /// </summary>
        private byte[] GetBytes<TSource>(TSource source)
        {
            var memory = new StringBuilder();
            using (var provider = new MemoryStockProvider(memory, _encoding))
            {
                IMappingProvider mapper = new JsonMappingProvider(provider);
                mapper.Serialize(source);
            }
            // Преобразуем в массив байт
            return _encoding.GetBytes(memory.ToString());
        }
        /// <summary>
        ///     Получить массив байт для вычисления хеша
        /// </summary>
        /// <param name="block">Контент-блок</param>
        /// <param name="lastHash">Последний хеш</param>
        /// <param name="salt">Криптографическая соль</param>
        /// <returns>Массив байт</returns>
        private byte[] GetDataForHash(ContentBlock block, string lastHash, string salt)
        {
            // Сериализуем
            var blockData = GetBytes(block);
            var lastHashData = Convert.FromBase64String(lastHash);
            var saltData = Convert.FromBase64String(salt);
            return InternalGetDataForHash(blockData, lastHashData, saltData);
        }
        /// <summary>
        ///     Смайнить хеш
        /// </summary>
        /// <param name="data"></param>
        /// <returns>(Хеш, криптографическая соль)</returns>
        private (byte[], byte[]) Mine(byte[] data, byte[] lastHash)
        {
            byte[] hash;
            byte[] salt;
            int saltNum = 0;
            do
            {
                saltNum++;
                salt = BitConverter.GetBytes(saltNum);
                // Создаем новый блок с учетом соли
                var newData = InternalGetDataForHash(data, lastHash, salt);
                // Считаем хеш
                hash = _CryptoService.ComputeHash(newData);
                // Проверяем, выполнилось ли условие останова майнинга
            } while (!CheckHashForPattern(Convert.ToBase64String(hash)));

            return (hash, salt);
        }
        /// <summary>
        ///     Получить массив байт для вычисления хеша
        /// </summary>
        private byte[] InternalGetDataForHash(byte[] block, byte[] lastHash, byte[] salt)
            => Merge(block, lastHash, salt);
        /// <summary>
        ///     Сконкатенировать массивы
        /// </summary>
        private byte[] Merge(params byte[][] arrs)
        {
            var length = arrs.Select(_ => _.Length).Sum();
            var result = new byte[length];
            var position = 0;
            foreach (var arr in arrs)
            {
                var count = arr.Length;
                Buffer.BlockCopy(arr, 0, result, position, count);
                position += count;
            }

            return result;
        }

        #endregion Private methods

        #region Consts

        // Паттерн, необходимый для завершения майнинга
        private const string PATTERN = "0";

        #endregion Consts

        #region Private fields

        private readonly IDBService _DBService;
        private readonly ICryptoService _CryptoService;
        private readonly IConfigService _ConfigService;

        private static Encoding _encoding => Encoding.UTF8;

        #endregion Private fields
    }
}