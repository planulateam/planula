﻿using System;
using System.Collections.Generic;
using System.Text;
using Planula.Models;

namespace Planula.Services.Miner
{
    /// <summary>
    ///     Сервис майнинга блоков
    /// </summary>
    public interface IMinerService
    {
        /// <summary>
        ///     Создать новый блок транзакции
        /// </summary>
        /// <returns>Блок данных типа <see cref="Block"/></returns>
        Block CreateNewBlock(ContentBlock content);
        /// <summary>
        ///     Проверить блок на валидность
        /// </summary>
        /// <param name="block">Блок данных</param>
        /// <param name="errors">Список ошибок, в случае провальной проверки валидации</param>
        /// <returns>Результат проверки</returns>
        bool CheckBlock(Block block, out List<Error> errors);
    }
}
