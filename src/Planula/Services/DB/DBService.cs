﻿using Planula.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.DB
{
    /// <summary>
    ///     Сервис связи с базой данных
    /// </summary>
    public class DBService : IDBService
    {
        #region .Ctor

        public DBService()
        {
        }

        #endregion .Ctor

        public List<Block> Blocks { get; } = new List<Block>();
    }
}
