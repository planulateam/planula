﻿using Planula.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Services.DB
{
    /// <summary>
    ///     Сервис связи с базой данных
    /// </summary>
    public interface IDBService
    {
        List<Block> Blocks { get; }
    }
}
