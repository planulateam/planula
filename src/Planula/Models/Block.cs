﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models
{
    /// <summary>
    ///     Блок транзакции
    /// </summary>
    public class Block
    {
        /// <summary>
        ///     Контент
        /// </summary>
        public ContentBlock Content { get; set; }
        
        // Для подтверждения авторства

        /// <summary>
        ///     Автор контента (публичный ключ в формате Base64)
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        ///     Отсоединенная подпись по хешу от контент-блока
        /// </summary>
        public string Sign { get; set; }

        // Для реализации блокчейна

        /// <summary>
        ///     Хеш блока
        /// </summary>
        public string Hash { get; set; }
        /// <summary>
        ///     Криптографическая соль
        /// </summary>
        public string Salt { get; set; }
    }
}
