﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models
{
    public class TransportBlock
    {
        /// <summary>
        ///     Блок транзакции
        /// </summary>
        public Block Body { get; set;  }

        /// <summary>
        ///     Отправитель
        /// </summary>
        public Node Sender { get; set; }

        /// <summary>
        ///     Получатель
        /// </summary>
        public Node Reciever { get; set; }
    }
}
