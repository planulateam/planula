using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Planula.Models
{
    /// <summary>
    ///     ���������� �� ����
    /// </summary>
    public class Node
    {
        public string IpAddress
        {
            get => Ip.ToString();
            set => Ip = IPAddress.Parse(value);
        }
        [JsonIgnore]
        public IPAddress Ip { get; set; }
        public ushort Port { get; set; }
    }
}
