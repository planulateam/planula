﻿using Planula.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models.Config
{
    /// <summary>
    ///     Известные узлы
    /// </summary>
    [ConfigDefault("known-nodes.json")]
    public class KnownNodesConfig
    {
        public List<Node> Nodes { get; set; }
    }
}
