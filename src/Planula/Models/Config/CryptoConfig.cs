﻿using Planula.Attributes;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Planula.Models.Config
{
    /// <summary>
    ///     Настройки криптосервиса
    /// </summary>
    [ConfigDefault("keys.json")]
    public class CryptoConfig
    {
        public RSAParameters PublicKey { get; set; }
        public RSAParameters PrivateKey { get; set; }
    }
}
