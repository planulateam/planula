﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models.Config
{
    /// <summary>
    ///     Общие настройки приложения
    /// </summary>
    public class AppConfig
    {
        /// <summary>
        ///     Настройки криптосервиса
        /// </summary>
        public CryptoConfig Crypto { get; set; }

        /// <summary>
        ///     Настройки транспорта
        /// </summary>
        public KnownNodesConfig KnownNodes { get; set; }
    }
}
