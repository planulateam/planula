﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models
{
    /// <summary>
    ///     Типы сообщений логгирования
    /// </summary>
    public enum ELogType
    {
        /// <summary>
        ///     Ошибка
        /// </summary>
        ERROR,
        /// <summary>
        ///     Предупреждение
        /// </summary>
        WARNING,
        /// <summary>
        ///     Информативное сообщение
        /// </summary>
        INFO,
    }
}
