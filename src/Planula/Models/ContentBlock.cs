﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models
{
    /// <summary>
    ///     Блок данных с контентом
    /// </summary>
    public class ContentBlock
    {
        /// <summary>
        ///     Контент в формате Base64
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        ///     Дата создания блока
        /// </summary>
        public DateTime Created { get; set; }
    }
}
