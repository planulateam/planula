﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Models
{
    /// <summary>
    ///     Ошибка
    /// </summary>
    public class Error
    {
        #region .Ctor

        public Error(string mess = "")
        {
            Message = mess;
        }

        #endregion .Ctor

        #region Public properties

        /// <summary>
        ///     Сообщение об ошибке
        /// </summary>
        public string Message { get; set; }

        #endregion Public properties
    }
}
