﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер потоковых выходящих данных
    /// </summary>
    public class StockStreamProvider : AStreamProvider, IStockStreamProvider
    {
        #region .Ctor
        
        public StockStreamProvider(Stream stream)
            : base(stream)
        { }

        #endregion .Ctor

        #region Public methods

        #region Implementation IStockStreamProvider

        /// <summary>
        ///     Записать из потока данных
        /// </summary>
        /// <param name="stream">Поток данных для чтения</param>
        public void WriteFrom(Stream stream)
        {
            int read;
            byte[] buffer = new byte[BUFF_LIMIT];
            while ((read = stream.Read(buffer, 0, BUFF_LIMIT)) > 0)
            {
                _stream.Write(buffer, 0, read);
            }
        }

        #endregion Implementation IStockStreamProvider

        #endregion Public methods
    }
}