﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер выходящих данных в файл
    /// </summary>
    public class FileStockProvider : StockStreamProvider
    {
        #region .Ctor

        public FileStockProvider(string path)
        : base((new StreamWriter(path)).BaseStream)
        { }

        public FileStockProvider(Stream stream)
            : base(stream)
        { }

        #endregion .Ctor

        #region Public methods

        /// <summary>
        ///     Сохранить последовательность байт
        /// </summary>
        /// <param name="arr">Последовательность байт</param>
        public void SetBytes(byte[] arr)
        {
            int read = 0;
            int length = 0;
            do
            {
                byte[] buffer = arr.Skip(read).Take(BUFF_LIMIT).ToArray();
                length = buffer.Length;
                read += length;
                _stream.Write(buffer, 0, length);
            } while (read > 0);
        }

        #endregion Public methods
    }
}