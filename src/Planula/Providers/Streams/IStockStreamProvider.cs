﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер потоковых выходящих данных
    /// </summary>
    public interface IStockStreamProvider : IStreamProvider
    {
        /// <summary>
        ///     Записать из потока данных
        /// </summary>
        /// <param name="stream">Поток данных для чтения</param>
        void WriteFrom(Stream stream);
    }
}