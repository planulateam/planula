﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер потоковых данных
    /// </summary>
    public interface IStreamProvider : IDisposable
    {
        /// <summary>
        ///     Поток данных
        /// </summary>
        Stream Stream { get; }
    }
}