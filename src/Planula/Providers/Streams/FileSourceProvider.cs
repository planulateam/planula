﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер входных данных из файла
    /// </summary>
    public class FileSourceProvider : SourceStreamProvider
    {
        #region .Ctor

        public FileSourceProvider(string path)
            : base((new StreamReader(path)).BaseStream)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Файл \"{path}\" не существует", path);
            }
            _filePath = path;
        }

        public FileSourceProvider(Stream stream)
            : base(stream)
        { }

        #endregion .Ctor

        #region Public methods

        /// <summary>
        ///     Получить массив байт
        /// </summary>
        /// <returns>Массив байт</returns>
        public byte[] GetBytes()
        {
            byte[] buffer = new byte[BUFF_LIMIT];
            var ans = new List<byte>();
            int read;
            while ((read = _stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                if (read != BUFF_LIMIT)
                    ans.AddRange(buffer.Take(read));
            }
            return ans.ToArray();
        }

        #endregion Public methods

        #region Private fields

        private string _filePath;

        #endregion Private fields
    }
}