﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер потоковых входящих данных
    /// </summary>
    public class SourceStreamProvider : AStreamProvider, ISourceStreamProvider
    {
        #region .Ctor

        public SourceStreamProvider(Stream stream)
            : base(stream)
        { }

        #endregion .Ctor

        #region Public methods

        #region Implementation ISourceStreamProvider

        /// <summary>
        ///     Прочитать в поток данных
        /// </summary>
        /// <param name="stream">Поток данных для записи</param>
        public void ReadTo(Stream stream)
        {
            int read;
            byte[] buffer = new byte[BUFF_LIMIT];
            while ((read = _stream.Read(buffer, 0, BUFF_LIMIT)) > 0)
            {
                stream.Write(buffer, 0, read);
            }
        }

        #endregion Implementation ISourceStreamProvider

        #endregion Public methods
    }
}