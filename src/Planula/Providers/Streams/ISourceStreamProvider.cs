﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Провайдер входных данных в виде байтовой последовательности
    /// </summary>
    public interface ISourceStreamProvider : IStreamProvider
    {
        /// <summary>
        ///     Прочитать в поток данных
        /// </summary>
        /// <param name="stream">Поток данных для записи</param>
        void ReadTo(Stream stream);
    }
}