﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Planula.Providers.Streams
{
    /// <summary>
    ///     Базовый провайдер потоковых данных
    /// </summary>
    public abstract class AStreamProvider : IStreamProvider
    {
        #region .Ctor

        public AStreamProvider(Stream stream)
        {
            _stream = stream;
            _isDisposed = false;
        }

        #endregion .Ctor

        #region Public properties

        #region Implementation IStreamProvider

        /// <summary>
        ///     Поток данных
        /// </summary>
        public Stream Stream => _stream;

        #endregion Implementation IStreamProvider

        #endregion Public properties

        #region Public methods

        #region Implementation IStreamProvider

        public virtual void Dispose()
        {
            if (!_isDisposed)
            {
                _stream.Dispose();
                _isDisposed = true;
            }
        }

        #endregion Implementation IStreamProvider

        #endregion Public methods

        #region Private fields

        protected Stream _stream;
        protected bool _isDisposed;

        #endregion Private fields

        #region Consts

        protected const int BUFF_LIMIT = 16 * 1024;

        #endregion Consts
    }
}