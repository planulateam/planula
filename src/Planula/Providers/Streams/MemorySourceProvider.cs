﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Planula.Providers.Streams
{
    public class MemorySourceProvider : SourceStreamProvider
    {
        /// <summary>
        ///     Провайдер входных данных из памяти
        /// </summary>
        /// <param name="source">Массив байт источника</param>
        public MemorySourceProvider(byte[] source)
            : base(new MemoryStream(source))
        { }
        /// <summary>
        ///     Провайдер входных данных из памяти
        /// </summary>
        /// <param name="source">Источник</param>
        public MemorySourceProvider(string source, Encoding encoding)
            : base(new MemoryStream(encoding.GetBytes(source)))
        { }
    }
}