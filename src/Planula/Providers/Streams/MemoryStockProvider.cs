﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Planula.Providers.Streams
{
    public class MemoryStockProvider : StockStreamProvider
    {
        /// <summary>
        ///     Провайдер выходнящих данных в память
        /// </summary>
        /// <param name="source">Массив байт, для записи</param>
        public MemoryStockProvider(Encoding encoding)
            : base(new MemoryStockStream(new StringBuilder(), encoding))
        { }

        /// <summary>
        ///     Провайдер выходнящих данных в память
        /// </summary>
        /// <param name="source">Массив байт, для записи</param>
        public MemoryStockProvider(StringBuilder stock, Encoding encoding)
            : base(new MemoryStockStream(stock, encoding))
        { }

        /// <summary>
        ///     Данные памяти
        /// </summary>
        public string Stock => (_stream as MemoryStockStream).Memory;

        /// <summary>
        ///     Поток для записи данных в память
        /// </summary>
        class MemoryStockStream : Stream
        {
            #region .Ctor

            public MemoryStockStream(StringBuilder stock, Encoding encoding)
            {
                _stock = stock;
                _encoding = encoding;
            }

            #endregion .Ctor

            #region Public properties

            public string Memory => _stock.ToString();

            #region Implementation Stream

            public override bool CanRead => false;

            public override bool CanSeek => false;

            public override bool CanWrite => true;

            public override long Length => _stock.Length;

            public override long Position { get => _position; set => _position = (int)value; }

            #endregion Implementation Stream

            #endregion Public properties

            #region Public methods

            #region Implementation Stream

            public override void Flush()
            { }

            public override int Read(byte[] buffer, int offset, int count)
            {
                throw new NotImplementedException();
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new NotImplementedException();
            }

            public override void SetLength(long value)
            {
                throw new NotImplementedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                if (_position < _stock.Length)
                {
                    _stock.Remove(_position, _stock.Length - _position);
                }
                _stock.Append(_encoding.GetString(buffer, offset, count));
                _position += count;
            }

            #endregion Implementation Stream

            #endregion Public methods

            #region Private fields

            private StringBuilder _stock;
            private Encoding _encoding;
            private int _position;

            #endregion Private fields
        }
    }
}