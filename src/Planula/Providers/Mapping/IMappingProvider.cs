﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Providers.Mapping
{
    /// <summary>
    ///     Провайдер данных
    /// </summary>
    public interface IMappingProvider
    {
        /// <summary>
        ///     Сериализовать объект
        /// </summary>
        /// <typeparam name="TSource">Тип объекта</typeparam>
        /// <returns>Сериализованный объект</returns>
        void Serialize<TSource>(TSource src);

        /// <summary>
        ///     Десериализовать объект
        /// </summary>
        /// <typeparam name="TStock">Тип объекта</typeparam>
        /// <returns>Десериализованный объект</returns>
        TStock Deserialize<TStock>();

        /// <summary>
        ///     Десериализовать объект
        /// </summary>
        /// <param name="stockType">Тип объекта</param>
        /// <returns>Десериализованный объект</returns>
        object Deserialize(Type stockType);
    }
}