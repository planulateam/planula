﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Planula.Providers.Streams;

namespace Planula.Providers.Mapping
{
    /// <summary>
    ///     Меппер JSON данных
    /// </summary>
    public class JsonMappingProvider : IMappingProvider
    {
        #region .Ctor

        public JsonMappingProvider(AStreamProvider stream)
        {
            _stream = stream;
        }

        #endregion .Ctor

        #region Public mehtods

        /// <summary>
        ///     Сериализовать объект
        /// </summary>
        /// <typeparam name="TSource">Тип объекта</typeparam>
        /// <returns>Сериализованный объект</returns>
        public void Serialize<TSource>(TSource src)
        {
            using (var textWriter = new StreamWriter(_stream.Stream, _encoding))
            {
                var serializer = CreateSerializer();
                serializer.Serialize(textWriter, src);
            }
        }
        /// <summary>
        ///     Десериализовать объект
        /// </summary>
        /// <typeparam name="TStock">Тип объекта</typeparam>
        /// <returns>Десериализованный объект</returns>
        public TStock Deserialize<TStock>()
        {
            TStock result;

            // TODO: добавить проверку совпадения класса и схемы
            using (var textReader = new StreamReader(_stream.Stream, _encoding))
            using (var jsonReader = new JsonTextReader(textReader))
            {
                var serializer = CreateSerializer();
                result = serializer.Deserialize<TStock>(jsonReader);
            }

            return result;
        }
        /// <summary>
        ///     Десериализовать объект
        /// </summary>
        /// <param name="stockType">Тип объекта</param>
        /// <returns>Десериализованный объект</returns>
        public object Deserialize(Type stockType)
        {
            object result = null;

            // TODO: добавить проверку совпадения класса и схемы
            using (var textReader = new StreamReader(_stream.Stream, _encoding))
            using (var jsonReader = new JsonTextReader(textReader))
            {
                var serializer = CreateSerializer();
                result = serializer.Deserialize(jsonReader, stockType);
            }

            return result;
        }

        #endregion Public mehtods

        #region Private methods

        private static JsonSerializer CreateSerializer()
            => JsonSerializer.Create(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Formatting = Formatting.Indented,
                Converters = { new StringEnumConverter() }
            });


        #endregion Private methods

        #region Private properties

        private static Encoding _encoding => Encoding.UTF8;

        #endregion Private properties

        #region Private fields

        private readonly AStreamProvider _stream;

        #endregion Private fields

    }
}
