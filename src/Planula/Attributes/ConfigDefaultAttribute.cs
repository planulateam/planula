﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Planula.Attributes
{
    /// <summary>
    ///     Дефолтное имя конфиг файла
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigDefaultAttribute : Attribute
    {
        #region .Ctor

        public ConfigDefaultAttribute(string defaultName)
        {
            Name = defaultName;
        }

        #endregion .Ctor

        #region Public properties

        public string Name { get; set; }

        #endregion Public properties
    }
}
