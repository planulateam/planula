using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using CLI.Helpers;
using CLI.Helpers.Exceptions;
using CLI.Models;
using NUnit.Framework;

namespace CLITests.Helpers
{
    public class CommandParserTests
    {
        #region Init

        public CommandParserTests()
        {
            _CommandParser = new CommandParser();
        }

        [SetUp]
        public void Setup()
        {

        }

        #endregion Init

        #region Tests

        [Test]
        public void Parse_SimpleCommand_Success()
        {
            var cmd = "command";
            string key1 = "j", val1 = "some1";
            string key2 = "key", val2 = "some2";
            string key3 = "k";
            var line = $"{cmd} {formatKey(key1)} {val1} {formatKey(key2)} {val2} {formatKey(key3)}";
            var expected = new CLIParseResult
            {
                Command = cmd,
                Args = new Dictionary<string, string>
                {
                    { key1, val1 },
                    { key2, val2 },
                    { key3, null },
                }
            };

            var result = Parse(line);

            Assert.AreEqual(expected.Command, result.Command);
            Assert.AreEqual(expected.Args, result.Args);
        }

        [Test]
        public void Parse_ManySpaces_Success()
        {
            var cmd = "command";
            string key1 = "j", val1 = "some1";
            string key2 = "key", val2 = "some2";
            var line = $"{cmd}    {formatKey(key1)} {val1}    {formatKey(key2)} {val2}";
            var expected = new CLIParseResult
            {
                Command = cmd,
                Args = new Dictionary<string, string>
                {
                    { key1, val1 },
                    { key2, val2 },
                }
            };

            var result = Parse(line);

            Assert.AreEqual(expected.Command, result.Command);
            Assert.AreEqual(expected.Args, result.Args);
        }

        [Test]
        public void Parse_Quotes_Success()
        {
            var cmd = "command";
            string key1 = "j", val1 = "someass   dd1";
            string key2 = "key", val2 = "some2 asdfas asdf";
            var line = $"{cmd} {formatKey(key1)} \'{val1}\' {formatKey(key2)} \"{val2}\"";
            var expected = new CLIParseResult
            {
                Command = cmd,
                Args = new Dictionary<string, string>
                {
                    { key1, val1 },
                    { key2, val2 },
                }
            };

            var result = Parse(line);

            Assert.AreEqual(expected.Command, result.Command);
            Assert.AreEqual(expected.Args, result.Args);
        }

        [TestCase("-key")]
        [TestCase("--k")]
        [TestCase("-")]
        [TestCase("--")]
        [TestCase("--some-0")]
        public void Parse_Key_Error(string key)
        {
            var line = $"command {key} some";
            var expected = new CLIParserException(CommandParser.KEY_NAME_ERR(key));

            Parse(line, expected);
        }

        [Test]
        public void Parse_KeyUnic_Error()
        {
            var key = "key";
            var line = $"command {formatKey(key)} some {formatKey(key)} some2";
            var expected = new CLIParserException(CommandParser.ARGUMENTS_KEY_UNIC_ERR(key));

            Parse(line, expected);
        }

        [Test]
        public void Parse_UnclosedQuotes_Error()
        {
            var key = "key";
            var line = $"command {formatKey(key)} \'some heh ";
            var expected = new CLIParserException(CommandParser.UNCLOSED_QUOTES_ERR("some heh "));

            Parse(line, expected);
        }


        [Test]
        public void Parse_ValueAfterCommand_Error()
        {
            var value = "value";
            var line = $"command {value}";
            var expected = new CLIParserException(CommandParser.VALUE_WITHOUT_KEY_ERR(value));

            Parse(line, expected);
        }

        #endregion Tests

        #region Private methods

        private CLIParseResult Parse(string line, CLIParserException exception = null)
        {
            CLIParseResult result = null;
            try
            {
                result = _CommandParser.Parse(line);
            }
            catch (CLIParserException e)
            {
                if (exception != null)
                {
                    Assert.AreEqual(exception.Message, e.Message);
                }
                else
                {
                    Assert.Fail();
                }
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
            return result;
        }

        private string formatKey(string key)
            => key.Length > 1 ? $"--{key}" : $"-{key}";

        #endregion Private methods

        #region Consts

        private const long TICKS = 100000000;

        #endregion Consts

        #region Private fields

        private readonly ICommandParser _CommandParser;

        #endregion Private fields
    }
}