﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Attributes
{
    /// <summary>
    ///     Аттрибут для аргумента команды CLI
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class CLIArgumentAttribute : Attribute
    {
        #region .Ctor

        public CLIArgumentAttribute(string name, string shortName)
        {
            Name = name;
            ShortName = shortName;
        }

        #endregion .Ctor

        #region Public fields

        /// <summary>
        ///     Ключ аргумента, идущее с двойным префикс-дефисом.
        ///     Пример: "--cart"
        /// </summary>
        public string Name { get; }
        /// <summary>
        ///     Ключ аргумента, идущее с одинарным префикс-дефисом.
        ///     Пример: "-c"
        /// </summary>
        public string ShortName { get; }

        #endregion Public fields
    }
}