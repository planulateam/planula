﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Attributes
{
    /// <summary>
    ///     Аттрибут для команды CLI
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Parameter)]
    public class CLIDescriptionAttribute : Attribute
    {
        #region .Ctor

        public CLIDescriptionAttribute(string description)
        {
            Description = description;
        }

        #endregion .Ctor

        #region Public fields

        /// <summary>
        ///     Текстовое описание элемента
        /// </summary>
        public string Description { get; }

        #endregion Public fields
    }
}