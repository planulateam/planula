﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Attributes
{
    /// <summary>
    ///     Аттрибут для команды CLI
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CLIMehodAttribute : Attribute
    {
        #region .Ctor

        public CLIMehodAttribute(string commandName)
        {
            CommandName = commandName;
        }

        #endregion .Ctor

        #region Public fields

        public string CommandName { get; }

        #endregion Public fields
    }
}