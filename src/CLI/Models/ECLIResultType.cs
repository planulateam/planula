﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Models
{
    /// <summary>
    ///     Типы результата выполнения команд
    /// </summary>
    public enum ECLIResultType
    {
        /// <summary>
        ///     Ошибка
        /// </summary>
        ERROR,
        /// <summary>
        ///     Предупреждение
        /// </summary>
        WARNING,
        /// <summary>
        ///     Информативное сообщение
        /// </summary>
        INFO,
        /// <summary>
        ///     Успешное выполнение
        /// </summary>
        SUCCESS
    }
}
