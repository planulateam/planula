﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Models
{
    /// <summary>
    ///     Результат выполнения команды
    /// </summary>
    public class CLICommandResult
    {
        /// <summary>
        ///     Тип команды
        /// </summary>
        public ECLIResultType Type { get; set; }
        /// <summary>
        ///     Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
