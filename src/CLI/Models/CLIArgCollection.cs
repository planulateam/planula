﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Models
{
    /// <summary>
    ///     Коллекция аргументов
    /// </summary>
    public class CLIArgCollection
    {
        /// <summary>
        ///     Флаги
        /// </summary>
        public List<string> Flags { get; set; }
        /// <summary>
        ///     Аргументы
        /// </summary>
        public Dictionary<string, string> Arguments { get; set; }
    }
}
