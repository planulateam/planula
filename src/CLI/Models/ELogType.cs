﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Models
{
    /// <summary>
    ///     Типы сообщений логгирования
    /// </summary>
    public enum ELogType
    {
        /// <summary>
        ///     Информативное сообщение
        /// </summary>
        INFO,
        /// <summary>
        ///     Предупреждение
        /// </summary>
        WARNING,
        /// <summary>
        ///     Ошибка
        /// </summary>
        ERROR,
        /// <summary>
        ///     Фатальная ошибка
        /// </summary>
        FATAL_ERROR,
    }
}
