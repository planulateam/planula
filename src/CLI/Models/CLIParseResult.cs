﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Models
{
    /// <summary>
    ///     Результат парсинга команды
    /// </summary>
    public class CLIParseResult
    {
        /// <summary>
        ///     Комманда
        /// </summary>
        public string Command { get; set; }
        /// <summary>
        ///     Аргументы
        /// </summary>
        public Dictionary<string, string> Args { get; set; }
    }
}
