﻿using CLI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Helpers
{
    /// <summary>
    ///     Помощик в выводе сообщения
    /// </summary>
    public interface IConsoleHelper
    {
        /// <summary>
        ///     Считать строку
        /// </summary>
        /// <returns></returns>
        string Read();

        /// <summary>
        ///     Вывести сообщение об успешном выполнении
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Success(string message);
        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Info(string message);
        /// <summary>
        ///     Вывести сообщение о предупреждении
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Warn(string message);
        /// <summary>
        ///     Вывести сообщение об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Error(string message);
        /// <summary>
        ///     Написать сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип сообщения</param>
        void Write(string message, ECLIResultType type);
        /// <summary>
        ///     Написать сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="color">Цвет сообщения</param>
        void Write(string message, ConsoleColor color);
    }
}