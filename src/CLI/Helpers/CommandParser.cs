﻿using CLI.Helpers.Exceptions;
using CLI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CLI.Helpers
{
    /// <summary>
    ///     Парсер команд
    /// </summary>
    public class CommandParser : ICommandParser
    {
        /// <summary>
        ///     Спарсить команду из строки
        /// </summary>
        /// <param name="line">Входная строка</param>
        /// <returns>Команда с аргументами</returns>
        public CLIParseResult Parse(string line)
        {
            line = line.Replace("\"", "\'");
            // Проверяем на правильную последовательность кавычек
            var quotesCount = line.Count(_ => _ == '\'');
            var arrs = new List<string>();
            var quoteValues = new HashSet<string>();
            bool needSeparator = false;
            while (line.Length > 0)
            {
                if (line.First() == ' ')
                {
                    var end = 0;
                    while (end < line.Length && line[end] == ' ')
                    {
                        end++;
                    }
                    line = line.Substring(end);
                    needSeparator = false;
                }
                else if (needSeparator)
                {
                    throw new CLIParserException(WRANG_ARGS_ERR(line));
                }
                else if (line.First() == '\'')
                {
                    line = line.Substring(1);
                    var end = line.IndexOf('\'');
                    if (end == -1)
                    {
                        throw new CLIParserException(UNCLOSED_QUOTES_ERR(line));
                    }
                    var arg = line.Substring(0, end);
                    arrs.Add(arg);
                    quoteValues.Add(arg);
                    line = line.Substring(end + 1);
                    needSeparator = true;
                }
                else
                {
                    var end = line.IndexOf(' ');
                    if (end == -1)
                    {
                        end = line.Length;
                    }
                    var arg = line.Substring(0, end);
                    arrs.Add(arg);
                    line = line.Substring(end);
                    needSeparator = true;
                }
            }

            var cmd = arrs[0];
            var args = ParseArgs(arrs.Skip(1).ToList(), quoteValues);

            return new CLIParseResult
            {
                Command = cmd,
                Args = args,
            };
        }

        #region Private methods

        /// <summary>
        ///     Разпарсить аргументы
        /// </summary>
        private Dictionary<string, string> ParseArgs(List<string> arr, HashSet<string> quoteValues)
        {
            var keys = new Dictionary<string, string>();
            string lastKey = "";
            for (int i = 0; i < arr.Count; i++)
            {
                var arg = arr[i];
                if (IsKey(arg))
                {
                    var key = ParseKey(arg);
                    if (keys.ContainsKey(key))
                    {
                        throw new CLIParserException(ARGUMENTS_KEY_UNIC_ERR(key));
                    }
                    keys.Add(key, null);
                    lastKey = key;
                }
                else if (IsValue(arg) || quoteValues.Contains(arg))
                {
                    if (keys.ContainsKey(lastKey))
                    {
                        keys[lastKey] = arg;
                    }
                    else
                    {
                        throw new CLIParserException(VALUE_WITHOUT_KEY_ERR(arg));
                    }
                }
                else
                {
                    throw new CLIParserException(KEY_NAME_ERR(arg));
                }
            }
            return keys;
        }

        /// <summary>
        ///     Спарсить ключ
        /// </summary>
        private string ParseKey(string key)
        {
            var end = 0;
            while (key[end] == '-')
            {
                end++;
            }
            key = key.Substring(end);
            return key;
        }

        /// <summary>
        ///     Проверить, что это ключ
        /// </summary>
        private bool IsKey(string arg)
            => IsShortKey(arg) || IsLongKey(arg);

        /// <summary>
        ///     Проверка, что ключ вида: "-k" или "-j"
        /// </summary>
        private bool IsShortKey(string arg)
            => new Regex($"-{ALPHABETIC_PATTERN}{{1}}").Match(arg).Value == arg;

        /// <summary>
        ///     Проверка, что ключ вида: "-key" или "-jam"
        /// </summary>
        private bool IsLongKey(string arg)
            => new Regex($"--{ALPHABETIC_PATTERN}{{2,}}").Match(arg).Value == arg;

        /// <summary>
        ///     Проверка, что значение только из латинских букв и цифр
        /// </summary>
        private bool IsValue(string arg)
            => new Regex($"{ALPHABETIC_PATTERN}+").Match(arg).Value == arg;

        /// <summary>
        ///     Проверить, что ключ состоит только из латинских букв
        /// </summary>
        private bool CheckIsAlphabetic(string key)
        {
            foreach (var c in key)
            {
                if (!((c > 'a' && c < 'z') || (c > 'A' && c < 'Z')))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion Private methods

        #region Messages

        public static string UNCLOSED_QUOTES_ERR(string line) => $"Отсутствует закрывающая кавычка: \"{line}\"";
        public static string WRANG_ARGS_ERR(string line) => $"Невозможно спарсить аргуметы: \"{line}\"";
        public static string ARGUMENTS_KEY_UNIC_ERR(string key) => "Были найдены совпадающие ключи: {key}";
        public static string KEY_NAME_ERR(string key) => $"Неправильное формат ключа: {key}";
        public static string VALUE_WITHOUT_KEY_ERR(string value) => $"Отсутствует ключ для значения: \"{value}\"";

        #endregion Messages

        #region Consts

        private const string ALPHABETIC_PATTERN = "[a-zA-Z0-9]";

        #endregion Consts
    }
}