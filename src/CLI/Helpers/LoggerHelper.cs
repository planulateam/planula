﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks.Dataflow;
using CLI.Models;

namespace CLI.Helpers
{
    /// <summary>
    ///     Сервис логгирования
    /// </summary>
    public class LoggerHelper : ILoggerHelper
    {
        #region .Ctor

        public LoggerHelper(IConsoleHelper consoleHelper)
        {
            _ConsoleHelper = consoleHelper;
        }

        #endregion .Ctor

        #region Public methods

        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Info(string message)
            => Write(message, ELogType.INFO);
        /// <summary>
        ///     Вывести сообщение о предупреждении
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Warn(string message)
            => Write(message, ELogType.WARNING);
        /// <summary>
        ///     Вывести сообщение об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Error(string message)
            => Write(message, ELogType.ERROR);
        /// <summary>
        ///     Вывести сообщение о фатальной ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void FatalError(string message)
            => Write(message, ELogType.FATAL_ERROR);

        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип сообщения</param>
        public void Write(string message, ELogType type)
        {
            // TODO: Добавить вывод отправителя
            _ConsoleHelper.Write($"[{type.ToString().ToUpper()}] | [{DateTime.Now.ToLongTimeString()}]: {message}", TypeMapping(type));
        }

        #endregion Public methods

        #region Private methods

        private static ECLIResultType TypeMapping(ELogType last)
            => last switch
            {
                ELogType.INFO => ECLIResultType.INFO,
                ELogType.WARNING => ECLIResultType.WARNING,
                ELogType.ERROR => ECLIResultType.ERROR,
                ELogType.FATAL_ERROR => ECLIResultType.ERROR,
                _ => ECLIResultType.INFO
            };

        #endregion Private methods

        #region Private fields

        private readonly IConsoleHelper _ConsoleHelper;

        #endregion Private fields
    }
}
