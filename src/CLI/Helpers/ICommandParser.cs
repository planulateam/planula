﻿using CLI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CLI.Helpers
{
    /// <summary>
    ///     Парсер команд
    /// </summary>
    public interface ICommandParser
    {
        /// <summary>
        ///     Спарсить команду из строки
        /// </summary>
        /// <param name="line">Входная строка</param>
        /// <returns>Команда с аргументами</returns>
        CLIParseResult Parse(string line);
    }
}