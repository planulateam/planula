﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Helpers.Exceptions
{
    public class CLIRunnerException : CLIException
    {
        public CLIRunnerException(string message)
            : base(message)
        { }
    }
}
