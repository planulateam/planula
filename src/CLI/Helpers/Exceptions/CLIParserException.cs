﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Helpers.Exceptions
{
    public class CLIParserException : CLIException
    {
        public CLIParserException(string message)
            : base(message)
        { }
    }
}
