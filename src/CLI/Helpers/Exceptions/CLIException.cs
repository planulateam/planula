﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Helpers.Exceptions
{
    public class CLIException : Exception
    {
        public CLIException(string message)
            : base(message)
        { }
    }
}
