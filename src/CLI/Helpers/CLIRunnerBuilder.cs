﻿using CLI.Attributes;
using CLI.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CLI.Helpers
{
    public class CLIRunnerBuilder
    {
        public CLIRunnerBuilder Init()
        {
            var consoleHelper = new ConsoleHelper();
            var commandParser = new CommandParser();
            var loggerHelper = new LoggerHelper(consoleHelper);
            _runner = new CLIRunner(consoleHelper, commandParser, loggerHelper);
            return this;
        }

        public CLIRunnerBuilder AddCommands<TMethodsSource>(TMethodsSource source)
            where TMethodsSource : class
        {
            var type = typeof(TMethodsSource);
            foreach(var cmd in type.GetMethods())
            {
                var attr = cmd.GetCustomAttribute<CLIMehodAttribute>();
                if (attr != null && cmd.ReturnType == typeof(CLICommandResult))
                {
                    _runner.Commands.Add(attr.CommandName, (cmd, source));
                }
            }

            return this;
        }

        /// <summary>
        ///     Закончить построение экземпляра
        /// </summary>
        /// <returns>Исполнитель коммандной строки</returns>
        public CLIRunner Build()
        {
            var result = _runner;
            _runner = null;
            return result;
        }

        #region Private fields

        private CLIRunner _runner;

        #endregion Private fields
    }
}
