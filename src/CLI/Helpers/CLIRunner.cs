﻿using CLI.Attributes;
using CLI.Helpers.Exceptions;
using CLI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CLI.Helpers
{
    /// <summary>
    ///     Исполнитель коммандной строки
    /// </summary>
    public class CLIRunner
    {
        #region .Ctor

        public CLIRunner(IConsoleHelper consoleHelper, ICommandParser commandParser, ILoggerHelper loggerHelper)
        {
            _ConsoleHelper = consoleHelper;
            _CommandParser = commandParser;
            _LoggerHelper = loggerHelper;
        }

        #endregion .Ctor

        #region Public methods

        public Dictionary<string, (MethodInfo, object)> Commands { get; } = new Dictionary<string, (MethodInfo, object)>();

        #endregion Public methods

        #region Public Methods

        public void Run()
        {
            while (true)
            {
                try
                {
                    ReadCommand();
                }
                catch (CLIException e)
                {
                    _LoggerHelper.Error(e.Message);
                }
                catch (Exception e)
                {
                    _LoggerHelper.FatalError(e.Message);
                    break;
                }
            }
        }

        #endregion Public Methods

        #region Private methods

        private void ReadCommand()
        {
            // Читаем строку
            var line = _ConsoleHelper.Read();
            // Парсим
            var parseResult = _CommandParser.Parse(line);
            // Проверяем на help
            if (CheckHelp(parseResult))
            {
                _LoggerHelper.Info(GetHelp());
                return;
            }
            // Ищем команду
            if (!Commands.ContainsKey(parseResult.Command))
            {
                throw new CLIRunnerException(UNKNOWN_COMMAND_ERR);
            }
            var (cmd, source) = Commands[parseResult.Command];
            // Проверяем на help
            if (CheckMethodHelp(parseResult))
            {
                _LoggerHelper.Info(GetHelp(cmd));
                return;
            }
            // Выполняем команду
            var result = InvokeCommand(cmd, source, parseResult.Args);
            // Выводим результат
            _ConsoleHelper.Write(result.Message, result.Type);
        }

        /// <summary>
        ///     Вызвать команду
        /// </summary>
        private CLICommandResult InvokeCommand(MethodInfo command, object obj, Dictionary<string, string> argsStr)
        {
            var args = new List<object>();
            var pars = command.GetParameters();
            foreach (var par in pars)
            {
                object arg = null;
                var attr = par.GetCustomAttribute<CLIArgumentAttribute>();
                if (argsStr.ContainsKey(attr.Name))
                {
                    arg = argsStr[attr.Name];
                }
                else if (argsStr.ContainsKey(attr.ShortName))
                {
                    arg = argsStr[attr.ShortName];
                }
                else if (par.HasDefaultValue)
                {
                    arg = par.DefaultValue;
                }
                if (arg == null)
                {
                    throw new CLIRunnerException($"Отсутствует параметр:\n {GetHelp(par)}");
                }
                args.Add(arg);
            }
            if (args.Count != pars.Length)
            {
                throw new CLIRunnerException($"Не все параметры указаны.\n {GetHelp(command)}");
            }
            return (command.Invoke(obj, args.ToArray()) as CLICommandResult);
        }

        /// <summary>
        ///     Проверить наличие запроса на вывод строки помощи
        /// </summary>
        private bool CheckHelp(CLIParseResult parseResult)
        {
            if (parseResult.Command.ToLower() == HELP_COMMAND)
            {
                if (parseResult.Args.Count > 0)
                {
                    throw new CLIRunnerException(HELP_ERR);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Проверить наличие запроса на вывод строки помощи для команды
        /// </summary>
        private bool CheckMethodHelp(CLIParseResult parseResult)
        {
            if (parseResult.Args.ContainsKey(HELP_SHORT_KEY) || parseResult.Args.ContainsKey(HELP_COMMAND))
            {
                if (parseResult.Args.Count != 1)
                {
                    throw new CLIRunnerException(HELP_ERR);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Получить строку помощи по всем методам
        /// </summary>
        private string GetHelp()
        {
            var result = "Доступен следующий набор методов:\n\n"
                + string.Join("\n\n", Commands.Values.Select(c => GetHelp(c.Item1)))
                + (Commands.Count() > 0 ? "\n" : "");
            return result;
        }

        /// <summary>
        ///     Получить строку помощи по методу
        /// </summary>
        private string GetHelp(MethodInfo method)
        {
            var attr = method.GetCustomAttribute<CLIMehodAttribute>();
            var attrDesc = method.GetCustomAttribute<CLIDescriptionAttribute>();
            var pars = method.GetParameters();
            // TODO: ДОбавить вывод примера
            var result = $"{attr.CommandName} - {attrDesc.Description}"
                + (pars.Count() > 0 ? "\n" : "")
                + string.Join("\n", pars.Select(p => GetHelp(p)));
            return result;
        }

        /// <summary>
        ///     Получить строку помощи по параметру
        /// </summary>
        private string GetHelp(ParameterInfo parameter)
        {
            var attr = parameter.GetCustomAttribute<CLIArgumentAttribute>();
            var attrDesc = parameter.GetCustomAttribute<CLIDescriptionAttribute>();
            string shortName = attr.ShortName, name = attr.Name, description = attrDesc.Description;
            return $"[-{shortName} \\ --{name}] - {description}"
                + (parameter.HasDefaultValue ? $" (по-умолчанию: \"{parameter.DefaultValue}\")" : "");
        }

        #endregion Private methods

        #region Messages

        public const string UNKNOWN_COMMAND_ERR = "Неизвестная команда";
        public const string HELP_ERR = "Ошибка вывода строки помощи";

        #endregion Messages

        #region Consts

        private const string HELP_COMMAND = "help";
        private const string HELP_SHORT_KEY = "h";

        #endregion Consts

        #region Private fields

        private readonly IConsoleHelper _ConsoleHelper;
        private readonly ICommandParser _CommandParser;
        private readonly ILoggerHelper _LoggerHelper;

        #endregion Private fields
    }
}
