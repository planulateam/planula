﻿using CLI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CLI.Helpers
{
    public class ConsoleHelper : IConsoleHelper
    {
        #region Public methods

        /// <summary>
        ///     Считать строку
        /// </summary>
        /// <returns></returns>
        public string Read()
        {
            Console.Write("> ");
            return Console.ReadLine();
        }

        /// <summary>
        ///     Вывести сообщение об успешном выполнении
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Success(string message)
            => Write(message, ECLIResultType.SUCCESS);
        /// <summary>
        ///     Вывести информативное сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Info(string message)
            => Write(message, ECLIResultType.INFO);
        /// <summary>
        ///     Вывести сообщение о предупреждении
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Warn(string message)
            => Write(message, ECLIResultType.WARNING);
        /// <summary>
        ///     Вывести сообщение об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Error(string message)
            => Write(message, ECLIResultType.ERROR);
        /// <summary>
        ///     Написать сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип сообщения</param>
        public void Write(string message, ECLIResultType type)
            => Write(message, ColorMapping(type));
        /// <summary>
        ///     Написать сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="color">Цвет сообщения</param>
        public void Write(string message, ConsoleColor color = DEFAULT_COLOR)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = DEFAULT_COLOR;
        }

        #endregion Public methods

        #region Private Methods

        private static ConsoleColor ColorMapping(ECLIResultType type)
            => type switch
            {
                ECLIResultType.SUCCESS => ConsoleColor.Green,
                ECLIResultType.ERROR => ConsoleColor.Red,
                ECLIResultType.WARNING => ConsoleColor.Yellow,
                ECLIResultType.INFO => DEFAULT_COLOR,
                _ => DEFAULT_COLOR,
            };

        #endregion Private Methods

        #region Consts

        private const ConsoleColor DEFAULT_COLOR = ConsoleColor.White;

        #endregion Consts
    }
}